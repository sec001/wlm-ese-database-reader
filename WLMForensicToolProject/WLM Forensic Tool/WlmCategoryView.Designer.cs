﻿/*
  WlmCategoryView.Designer.cs
	
  Windows Live Messenger Category View Designer
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: WlmCategoryView.Designer.cs, Joeri D. Blokhuis Exp $
 */
namespace TreeTestApp
{
    partial class ContactCategorieView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            CommonTools.TreeListColumn treeListColumn1 = new CommonTools.TreeListColumn("group", "Category");
            CommonTools.TreeListColumn treeListColumn2 = new CommonTools.TreeListColumn("status", "Status");
            CommonTools.TreeListColumn treeListColumn3 = new CommonTools.TreeListColumn("email", "Emailaddress");
            CommonTools.TreeListColumn treeListColumn4 = new CommonTools.TreeListColumn("nickname", "NickName");
            CommonTools.TreeListColumn treeListColumn5 = new CommonTools.TreeListColumn("status", "StatusMsg");
            CommonTools.TreeListColumn treeListColumn6 = new CommonTools.TreeListColumn("title", "Title");
            CommonTools.TreeListColumn treeListColumn7 = new CommonTools.TreeListColumn("firstname", "Firstname");
            CommonTools.TreeListColumn treeListColumn8 = new CommonTools.TreeListColumn("middlename", "Middlename");
            CommonTools.TreeListColumn treeListColumn9 = new CommonTools.TreeListColumn("lastname", "Lastname");
            CommonTools.TreeListColumn treeListColumn10 = new CommonTools.TreeListColumn("friendlyname", "Friendlyname");
            CommonTools.TreeListColumn treeListColumn11 = new CommonTools.TreeListColumn("gender", "Gender");
            CommonTools.TreeListColumn treeListColumn12 = new CommonTools.TreeListColumn("birthdate", "BirthDate");

            //emailaddresses
            CommonTools.TreeListColumn treeListColumn13 = new CommonTools.TreeListColumn("msn", "MSN");
            CommonTools.TreeListColumn treeListColumn14 = new CommonTools.TreeListColumn("home", "Home");
            CommonTools.TreeListColumn treeListColumn15 = new CommonTools.TreeListColumn("other", "Other");

            //WL
            CommonTools.TreeListColumn treeListColumn16 = new CommonTools.TreeListColumn("prof_lc", "ProfileLastChanged");
            CommonTools.TreeListColumn treeListColumn17 = new CommonTools.TreeListColumn("prof_ls", "ProfileLastSynced");
            CommonTools.TreeListColumn treeListColumn18 = new CommonTools.TreeListColumn("wl_cd", "WL_CreateDate");
            CommonTools.TreeListColumn treeListColumn19 = new CommonTools.TreeListColumn("wl_lc", "WL_LastChanged");
            CommonTools.TreeListColumn treeListColumn20 = new CommonTools.TreeListColumn("wl_pu", "WL_ProfileURL");
            //CommonTools.TreeListColumn treeListColumn21 = new CommonTools.TreeListColumn("other1", "Other");

            this.m_contact_category = new TreeTestApp.ContactCategoryViewTree();
            ((System.ComponentModel.ISupportInitialize)(this.m_contact_category)).BeginInit();
            this.SuspendLayout();
            // 
            // m_folderTree
            // 
            this.m_contact_category.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            treeListColumn1.AutoSizeMinSize = 0;
            treeListColumn1.Width = 250;
            
            //status
            treeListColumn2.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn2.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn2.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn2.Width = 40;
            
            treeListColumn3.AutoSizeMinSize = 0;
            treeListColumn3.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn3.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn3.Width = 126;
            treeListColumn4.AutoSizeMinSize = 0;
            treeListColumn4.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn4.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn4.Width = 126;
            treeListColumn5.AutoSizeMinSize = 0;
            treeListColumn5.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn5.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn5.Width = 126;
            
            //title
            treeListColumn6.AutoSizeMinSize = 0;
           // treeListColumn6.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            //treeListColumn6.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn6.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn6.Width = 50;

            treeListColumn7.AutoSizeMinSize = 0;
            //treeListColumn7.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            //treeListColumn7.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn7.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn7.Width = 126;

            treeListColumn8.AutoSizeMinSize = 0;
            //treeListColumn8.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn8.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn8.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn8.Width = 126;

            treeListColumn9.AutoSizeMinSize = 0;
            //treeListColumn9.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn9.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn9.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn9.Width = 126;

            treeListColumn10.AutoSizeMinSize = 0;
            //treeListColumn10.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn10.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn10.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn10.Width = 126;

            treeListColumn11.AutoSizeMinSize = 0;
            //treeListColumn11.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn11.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn11.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn11.Width = 126;

            treeListColumn12.AutoSizeMinSize = 0;
            //treeListColumn12.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            //treeListColumn12.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn12.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn12.Width = 126;

            treeListColumn13.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn13.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn13.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn13.Width = 100;

            treeListColumn14.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn14.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn14.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn14.Width = 100;

            treeListColumn15.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn15.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn15.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn15.Width = 130;

            treeListColumn16.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn16.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn16.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn16.Width = 130;

            treeListColumn17.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn17.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn17.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn17.Width = 130;

            treeListColumn18.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn18.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn18.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn18.Width = 170;

            treeListColumn19.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn19.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn19.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn19.Width = 170;

            treeListColumn20.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            treeListColumn20.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeListColumn20.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            treeListColumn20.Width = 200;

           // treeListColumn21.AutoSizeMinSize = 0;
            //treeListColumn2.CellFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255
            //treeListColumn2.CellFormat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
           // treeListColumn21.HeaderFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
           // treeListColumn21.CellFormat.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
           // treeListColumn21.Width = 100;


            this.m_contact_category.Columns.AddRange(new CommonTools.TreeListColumn[] {
            treeListColumn1,
            treeListColumn2,
            treeListColumn3,treeListColumn4,treeListColumn5,treeListColumn6,treeListColumn7,treeListColumn8,treeListColumn9,treeListColumn10,treeListColumn11,treeListColumn12,
            treeListColumn13,treeListColumn14,treeListColumn15,treeListColumn16,treeListColumn17,treeListColumn18,treeListColumn19,treeListColumn20});
            this.m_contact_category.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.m_contact_category.Images = null;
            this.m_contact_category.Location = new System.Drawing.Point(3, 3);
            this.m_contact_category.Name = "m_folderTree";
            this.m_contact_category.Size = new System.Drawing.Size(471, 326);
            this.m_contact_category.TabIndex = 0;
            this.m_contact_category.Text = "treeListView1";
            this.m_contact_category.ViewOptions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // FolderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_contact_category);
            this.Name = "FolderView";
            this.Size = new System.Drawing.Size(477, 332);
            ((System.ComponentModel.ISupportInitialize)(this.m_contact_category)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TreeTestApp.ContactCategoryViewTree m_contact_category;
    }
}

﻿/*
  WlmCategoryView.cs
	
  Graphical User Interface to display Windows Live Messenger user contacts in categories
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: WlmCategoryView.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using EsentReader.esent;

namespace TreeTestApp
{
    public partial class ContactCategorieView : UserControl
    {

        //constructor
        public ContactCategorieView()
        {
            InitializeComponent();
        }
        
        
        public void initNodes(String filename, String version){
            m_contact_category.AddCategories(filename, version);
        }

        public void clearAllCategories()
        {
            m_contact_category.RemoveCategories();
        }

        public void exportFolderView(int option)
        {
            m_contact_category.exportContactCategoryView(option);
        }
    }

    public class ContactCategoryViewTree : CommonTools.TreeListView
    {
        private EsentReader.esent.wlmTreeView wlmtree;
        public ContactCategoryViewTree()
        {
           
        }

        public void RemoveCategories()
        {
            Nodes.Clear();
        }

        public void exportContactCategoryView(int option)
        {
            wlmtree.createExportTable(option);
        }

        public void AddCategories(String filename, String version)
        {
            CommonTools.Node[] categoryNode = null;
            wlmtree = new wlmTreeView(version,filename);
            categoryNode = wlmtree.getCategories();

            for(int i = 0; i <categoryNode.Length; i++){
                try{
                    Nodes.Add(categoryNode[i]);
                }
                catch (System.NullReferenceException){
                }
            }

            if(version.Equals("2011")){
                categoryNode[(categoryNode.Length -1)].Nodes.Add(wlmtree.getUserInfo());
            }

            //Add contacts to each corresponding category

            //loop through each category (category count)
            //get category id = 583453453
            //get contacts for each category
            //keep list with you add to category 
            //whats left is for Other Contacts

            for (int j = 0; j < wlmtree.getCategoryLinkCount(); j++)
            {
                //get categoryId - i
                //get contactId - i
                int i = wlmtree.getCategoryLinkContactId(j);
                if (i != -1)
                {
                    categoryNode[wlmtree.getCategoryId(i,j)].Nodes.Add(wlmtree.getContactInfo(i));
                }
                else
                {
                    //MessageBox.Show("FAILURE");
                }
            }


            //add contacts to category
            for (int i = 0; i < wlmtree.getContactCount(); i++)
            {   
                //if(contactId != contactLinkId
                if (wlmtree.isInCategory(i) != -1)
                {
                    categoryNode[wlmtree.getCategoryId(i,-1)].Nodes.Add(wlmtree.getContactInfo(i));
                }
            }
        }

 
    }
}
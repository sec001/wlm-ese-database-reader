﻿/*
  FileChooser.cs
	
  File Dialog: let user pick a file
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: FileChooser.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class filechooser
    {
        private string defaultPath;

        public filechooser() {
            //select home directory based on platform
            this.defaultPath = (Environment.OSVersion.Platform == PlatformID.Unix ||
                       Environment.OSVersion.Platform == PlatformID.MacOSX)
                       ? Environment.GetEnvironmentVariable("HOME")
                       : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
        }

        /**
         *  openSelectFileDialog()
         *  Open a dialog to select a file
         *  pre:
         *  post: return filename
         */
        [STAThread]
        public string openSelectFileDialog()
        {
            
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = defaultPath;
            //set filter on files to select. Default .edb files only
            openFileDialog1.Filter = "All files (*.*)|(*.*)|edb files (*.edb)|*.edb";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
    
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                String file = openFileDialog1.FileName;
                this.defaultPath = Path.GetDirectoryName(file);

                return file;

            }
            return "";
        }
    }
}

﻿Reads any Extensible Storage Engine(ESE) database files(.edb). 
Uses of ESE databases are found in Windows Live Mail/Messenger/Desktop Search/Calendar/etc. 
WLM ESE Examiner detects Windows Live Messenger database files and shows all user contacts in the corresponding category. 
Further it is possible to compare database files, search on any field and export to CSV and XML file formats. 
This program is licensed under GNU General Public License version 3 and is ready to use and continued in its developments. 
Any suggestions/comments are more than welcome.

Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
Contact:
	wouter@dongit.nl
	jblokhuis@os3.nl
	http://sourceforge.net/projects/wlmeseexaminer/

The program has the following key features:
	- Read any ESE database file
	- Compare ESE database files(with synchronized view)
	- WLM Contact Categories overview(only with Windows Live Messenger)
	- Search on (multiple) tables with highlight of the result
	- Export complete database/table to CSV or XML


*********************************Read ESE files [Database tab]*********************************************
Any type of ESE database should be able to be loaded by the program.
Once a file is loaded, a list with table names are displayed on the left side. Each table can be selected
and displayed on the right side. Every selected table is placed in a tab to have multiple open at the same
time and for easy switching between tables.


*********************************Compare ESE files [Compare tab]*******************************************
Two ESE files(of any type) can be loaded and compared. When both files are loaded, the program will
run a check if the database are of the same type. Next a list of database tables will be displayed, which
can be selected to load the respective table of each database. When data in the right table does not
match with data from the left table it will highlight each row in yellow. The cell containing different
data will be highlighted in red. Scrolling through the table can be done in a synchronized way. When
scrolling the scrollbars on the right side the left table will automatically change its view to that of the
right one.


*********************************WLM Contact Categories [Contacts Categories tab]**************************
When an ESE file is opened the program checks for Windows Live Messenger (2009/2011). If a WLM
file is detected the program creates a contact list as can be found when signed into WLM.
Contact Categories will show each category with corresponding contacts.


*********************************Search on tables**********************************************************
A search function was added as a useful extension to make the database more accessible.
Three options can be selected when searching:
	1) Match – match query but ignores case-sensitive characters.
	2) All open tables – search in all tables opened by the user.
	3) Highlight last search – when doing multiple queries on one table it might be useful to keep
	your last search highlighted. A different cell color will be chosen to a maximum of 6
	consecutive queries.
A default search will show the result of a query of the current selected table and ignores case-sensitive
characters.

**********************************Export database**********************************************************
Databases can be exported in the following two formats: CSV and XML. The 'Database' tab allows
one to export all tables or the current selected table. Contact Categories can also be exported to both
formats when the tab-page is selected or from the menu-bar.
﻿/*
  ExportCSVForm.cs
	
  Export data to CSV file format Graphical User Interface(Form)
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: ExportCSVForm.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EsentReader;
using EsentReader.esent;

namespace EsentReader
{
    public partial class ExportToCSVForm : Form
    {
        private string tablename;
        private string filepath;
        private DataGridView dgv;
        private int option;

        public ExportToCSVForm(string tablename, string filepath, DataGridView dgv)
        {
            InitializeComponent();

            this.tablename = tablename;
            this.filepath = filepath;
            this.dgv = dgv;

            if (this.tablename == "")
            {
                radioButtonCTableCSV.Enabled = false;
            }
        }

        private void exportBtn_Click(object sender, EventArgs e)
        {
            if (radioButtonAllTblCSV.Checked)
            {
                this.option = 1;
                Export();
            }
            if (radioButtonCTableCSV.Checked)
            {
                this.option = 2;
                Export();
            }
            if (radioButtonCTableCSV.Checked == false && radioButtonAllTblCSV.Checked == false)
            {
                MessageBox.Show("Please select one of the export options");
            }
        }

        private void Export()
        {
            try
            {
                new eseExportToCsv().exportTables(this.tablename, this.filepath, this.option, this.dgv);
                MessageBox.Show("Finished exporting to CSV");
                this.Dispose();
            }
            catch (System.ArgumentOutOfRangeException)
            {

            }
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}

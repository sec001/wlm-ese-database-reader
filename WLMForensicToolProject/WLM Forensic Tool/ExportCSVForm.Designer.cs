﻿/*
  ExportCSVForm.Designer.cs
	
  Export data to CSV file format Designer environment
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: ExportCSVForm.Designer.cs, Joeri D. Blokhuis Exp $
 */
namespace EsentReader
{
    partial class ExportToCSVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportToCSVForm));
            this.labelCSV = new System.Windows.Forms.Label();
            this.radioButtonAllTblCSV = new System.Windows.Forms.RadioButton();
            this.radioButtonCTableCSV = new System.Windows.Forms.RadioButton();
            this.exportBtn = new System.Windows.Forms.Button();
            this.close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelCSV
            // 
            this.labelCSV.AutoSize = true;
            this.labelCSV.Location = new System.Drawing.Point(12, 9);
            this.labelCSV.Name = "labelCSV";
            this.labelCSV.Size = new System.Drawing.Size(106, 13);
            this.labelCSV.TabIndex = 0;
            this.labelCSV.Text = "Select export type(s):";
            // 
            // radioButtonAllTblCSV
            // 
            this.radioButtonAllTblCSV.AutoSize = true;
            this.radioButtonAllTblCSV.Location = new System.Drawing.Point(15, 59);
            this.radioButtonAllTblCSV.Name = "radioButtonAllTblCSV";
            this.radioButtonAllTblCSV.Size = new System.Drawing.Size(67, 17);
            this.radioButtonAllTblCSV.TabIndex = 1;
            this.radioButtonAllTblCSV.TabStop = true;
            this.radioButtonAllTblCSV.Text = "All tables";
            this.radioButtonAllTblCSV.UseVisualStyleBackColor = true;
            // 
            // radioButtonCTableCSV
            // 
            this.radioButtonCTableCSV.AutoSize = true;
            this.radioButtonCTableCSV.Location = new System.Drawing.Point(15, 36);
            this.radioButtonCTableCSV.Name = "radioButtonCTableCSV";
            this.radioButtonCTableCSV.Size = new System.Drawing.Size(85, 17);
            this.radioButtonCTableCSV.TabIndex = 2;
            this.radioButtonCTableCSV.TabStop = true;
            this.radioButtonCTableCSV.Text = "Current table";
            this.radioButtonCTableCSV.UseVisualStyleBackColor = true;
            // 
            // exportBtn
            // 
            this.exportBtn.Location = new System.Drawing.Point(15, 102);
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(75, 23);
            this.exportBtn.TabIndex = 3;
            this.exportBtn.Text = "export";
            this.exportBtn.UseVisualStyleBackColor = true;
            this.exportBtn.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // close
            // 
            this.close.Location = new System.Drawing.Point(114, 102);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 4;
            this.close.Text = "close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // ExportToCSVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(204, 137);
            this.Controls.Add(this.close);
            this.Controls.Add(this.exportBtn);
            this.Controls.Add(this.radioButtonCTableCSV);
            this.Controls.Add(this.radioButtonAllTblCSV);
            this.Controls.Add(this.labelCSV);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportToCSVForm";
            this.Text = "Export to CSV";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCSV;
        private System.Windows.Forms.RadioButton radioButtonAllTblCSV;
        private System.Windows.Forms.RadioButton radioButtonCTableCSV;
        private System.Windows.Forms.Button exportBtn;
        private System.Windows.Forms.Button close;
    }
}
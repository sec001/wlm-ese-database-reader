﻿/*
  GUI.cs
	
  Graphical User Interface
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: GUI.cs, Joeri D. Blokhuis Exp $
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EsentReader;
using EsentReader.esent;
using EsentReader.about;

namespace WindowsFormsApplication1
{
   
    public partial class GUI : Form
    {

        int _horizontalOffsetStop;

        private const int CLEAR_ALL = 0;
        private const int CLEAR_FOR_RELOAD = 1;

        private filechooser file_chooser;
        private String filepath = "";
        private eseDbInfo wlm_dbinfo;
        private TableReader ese_table_reader;
 
        private List<string> table_names;
        private TabPage[] tab_pages;
        private DataGridView[] ese_datatables;
        private DataTable ese_datatable;
        private TreeTestApp.ContactCategorieView wlm_categories_view;

        private int tab_count;
        private int total_tabs;
        private int keep_highlight_count = 0;
        private Color[] colors = new Color[6] { Color.LimeGreen, Color.Orange, Color.Aqua, Color.Magenta, Color.Tomato, Color.LightSlateGray }; 

        private const int XML = 1;
        private const int CSV = 0;

        private String cmp1_file, cmp2_file;
        

        /**
         * GUI()
         * Initialize the program and show GUI
         * 
         */
        public GUI()
        {
            InitializeComponent();
            file_chooser        = new filechooser();
            ese_table_reader    = new TableReader();
            wlm_dbinfo          = new eseDbInfo();

            //Set categoryview (WLM aggregated table)
            this.wlm_categories_view = new TreeTestApp.ContactCategorieView();
            tabPage3.Controls.Add(this.wlm_categories_view);
            tabPage3.Name = "WLM_Contact_Categories";
            tabPage3.Padding = new System.Windows.Forms.Padding(3);
            tabPage3.TabIndex = 1;
            tabPage3.Text = "WLM Contact Categories";
            tabPage3.UseVisualStyleBackColor = true;
   
            this.wlm_categories_view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wlm_categories_view.Name = "folderView1";
            this.wlm_categories_view.TabIndex = 0;
        }

        /*
         * Open an .ese file by the 'open' button
         */
        private void button1_Click(object sender, EventArgs e)
        {
            openFile();
        }

        /*
         * Open an .ese file by the 'open' menu in the menubar
         */
        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
           openFile();
        }

        /*
         * Export datatable to XML file(s)
         */
        private void button2_Click(object sender, EventArgs e)
        {
            exportToFile(XML);
        }


        /*
         * When clicked on a tablename - open or close a datatable
         */
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs table)
        {
            //count the amount of rows in the list of tableNames
            int table_count = ese_tablename_view.RowCount;
            
            //get tablename from row clicked
            String tablename = ese_tablename_view.Rows[table.RowIndex].Cells[1].FormattedValue.ToString();
            bool table_status = Convert.ToBoolean(ese_tablename_view.Rows[table.RowIndex].Cells[0].Value);
            Console.WriteLine(tablename + "/" + table_count);

            //check if already opened if that's the case close table
            if (table.ColumnIndex == 0 && table_status == true)
            {
                ese_tablename_view[0, table.RowIndex].Value = false;
                tabControl2.TabPages.Remove(this.tab_pages[table.RowIndex]);
            }
                    
            //open table in new tab
           if ((table.ColumnIndex == 0 || table.ColumnIndex == 1) && !table_status)
           {
               tab_pages[table.RowIndex] = new TabPage(tablename);
               tabControl2.TabPages.Add(this.tab_pages[table.RowIndex]);
               ese_datatables[table.RowIndex] = new DataGridView();
               ese_datatables[table.RowIndex].Dock = System.Windows.Forms.DockStyle.Fill;
               ese_datatables[table.RowIndex].AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
               ese_datatables[table.RowIndex].AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
               ese_datatables[table.RowIndex].ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
               ese_datatables[table.RowIndex].AllowUserToAddRows = false;
               this.tab_pages[table.RowIndex].Controls.Add(ese_datatables[table.RowIndex]);

               //get data from ese table to display in a new datagridview
               ese_datatables[table.RowIndex].DataSource = new EsentReader.esent.wlmReadTable(tablename, this.filepath).QueryESE();

               //add one to tabcount
               this.tab_count++;
               //set checkbox to selected
               ese_tablename_view[0, table.RowIndex].Value = true;
           }
        }

        /*
         *  Reset all tabpages and parameters to its initial value
         */
        private void button4_Click(object sender, EventArgs e)
        {
            resetItems(CLEAR_ALL);
        }

        /**
         *  Reset for RELOAD or ALL. 
         *  clears open tabpages, fileproperties, categoryview, 
         *  comparegrids, tablenamegrid, search results
         */
        private void resetItems(int option)
        {
            switch (option)
            {
              case CLEAR_ALL:
                    this.wlm_categories_view.clearAllCategories();
                    removeAllTabs();
                    this.filepath = "";
                    this.dataGridCompare1.DataSource = null;
                    this.dataGridCompare2.DataSource = null;
                    this.ese_tablename_view.DataSource = null;
                    label1.Text = "File name:";
                    label2.Text = "File size:";
                    label3.Text = "Amount of tables:";
                    searchResultsLabel.Text = "";
                break;

               case CLEAR_FOR_RELOAD:
                    this.wlm_categories_view.clearAllCategories();
                    removeAllTabs();
                    this.filepath = "";
                    this.ese_tablename_view.DataSource = null;
                    label1.Text = "File name:";
                    label2.Text = "File size:";
                    label3.Text = "Amount of tables:";
                break;
            }
        }

        /**
         *  When file is selected/loaded set its properties
         */
        private void setFilepropertiesOnLoad(string filepath)
        {
            label1.Text = "File name: " + filepath;
            long length = new System.IO.FileInfo(filepath).Length;
            length = length / 1024; //KB
            label2.Text = "File size: " + (length / 1024) + " MB";
        }

        /*
         *  Exit the program by the menubar
         */
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Application.Exit();
        }

        /*
         * Display the AboutBox via menubar
         */
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog();
        }

        /*
         *  Generic file opener
         *  open a .ese file and load
         */
        public void openFile(){
            //clear items before loading
            resetItems(CLEAR_FOR_RELOAD);

            //open file selector dialog
            this.filepath = file_chooser.openSelectFileDialog();
            if (!filepath.Equals(""))
            {
                //set file properties
                setFilepropertiesOnLoad(filepath);
                openFileButton.Enabled = true;

                ese_tablename_view.DataSource = null;
                ese_tablename_view.AllowUserToAddRows = false;

                //load and display all tablenames
                ese_tablename_view.DataSource = ese_table_reader.GetResultsTable(new EsentReader.esent.wlmReadTable("","").openDatabase(filepath, "off"),System.IO.Path.GetFileName(filepath));
          
                this.total_tabs = 0;
                this.total_tabs = ese_tablename_view.RowCount;
                tab_pages = new TabPage[this.total_tabs];
                ese_datatables = new DataGridView[this.total_tabs];

                label3.Text = "Amount of tables: " + this.total_tabs;

                //get WLM version if empty string is returned no categories are displayed
                string version = wlm_dbinfo.getVersion(ese_tablename_view);
                if (!version.Equals("") )
                {
                    //open CategoryView for WLM only
                    this.wlm_categories_view.initNodes(this.filepath,version);
                }
             }
        }

        /*
         *  Open file on the left pane to compare
         */
        private void openFileCompare1_Click(object sender, EventArgs e)
        {
            //open file selector
            this.filepath = file_chooser.openSelectFileDialog();
            if (!filepath.Equals(""))
            {
                this.cmp1_file = this.filepath;
                dataGridCompare1.DataSource = null;
                dataGridCompare1.AllowUserToAddRows = false;
                this.table_names = null;
                this.table_names = new EsentReader.esent.wlmReadTable("","").openDatabase(filepath, "off");

                int table_row_select = 0;

                //get WLM version if empty string is returned no categories are displayed
                string version = wlm_dbinfo.getVersion(this.table_names);
                if (!version.Equals(""))
                {
                    table_row_select = wlm_dbinfo.getTableRowByList(this.table_names);
                }

                //get datatable from database.
                ese_datatable = new DataTable();
                ese_datatable = new EsentReader.esent.wlmReadTable(this.table_names[table_row_select], this.filepath).QueryESE();
                dataGridCompare1.DataSource = ese_datatable;
               
                if (dataGridCompare1.DataSource != null && dataGridCompare2.DataSource != null)
                {
                    doCompare();
                    listBox1.DataSource = this.table_names;
                }
            }
        }

        /*
         *  Open file on the right pane to compare
         */ 
        private void openFileCompare2_Click(object sender, EventArgs e)
        {
            //open file selector
            this.filepath = file_chooser.openSelectFileDialog();
            if (!filepath.Equals(""))
            {
                //set everything to default
                this.cmp2_file = this.filepath;
                dataGridCompare2.DataSource = null;
                dataGridCompare2.AllowUserToAddRows = false;
                this.table_names = null;
                this.table_names = new EsentReader.esent.wlmReadTable("", "").openDatabase(filepath, "off");

                int table_row_select = 0;

                //get WLM version if empty string is returned no categories are displayed
                string version = wlm_dbinfo.getVersion(this.table_names);
                if (!version.Equals(""))
                {
                    table_row_select = wlm_dbinfo.getTableRowByList(this.table_names);
                }

                //get datatable from database.
                ese_datatable = new DataTable();
                ese_datatable = new EsentReader.esent.wlmReadTable(this.table_names[table_row_select], this.filepath).QueryESE();
                dataGridCompare2.DataSource = ese_datatable;

                //check if both datagrids have data, if so compare and show all tablenames
                if (dataGridCompare1.DataSource != null && dataGridCompare2.DataSource != null)
                {
                    doCompare();
                    listBox1.DataSource = this.table_names;
                }
            }

        }


        /**
         * doCompare()
         * Compare two datagrids and highlight the differences
         * A row is highlighted yellow when data does not match
         * The corresponding column(s) are highlighted red
         * 
         * pre: both datagrids should be loaded
         * post: right table highlighted
         */
        public void doCompare()
        {
            //get rowcount
            int cp1_row = dataGridCompare1.RowCount;
            int cp2_row = dataGridCompare2.RowCount;

            //get columncount
            int cp1_col = dataGridCompare1.ColumnCount;
            int cp2_col = dataGridCompare2.ColumnCount;

            int rows;
            int rows_left = -1;
            Boolean higlighted = false;


            if (cp1_row >= cp2_row)
            {
                rows = cp2_row;
            }
            else
            {
                rows = cp1_row;
                rows_left = cp2_row - rows;
            }
            //better to compare columnnames instead of checking the amount of columns only!
            if (cp1_col == cp2_col)
            {

                for (int i = 0; i < cp1_col; i++)
                {
                    higlighted = false;
                    for (int j = 0; j < rows; j++)
                    {
                        try
                        {
                            //when data is not equal, highlight row and color the cell
                            if (!dataGridCompare1[i, j].Value.ToString().Equals(dataGridCompare2[i, j].Value.ToString()))
                            {
                               
                                if (higlighted == true)
                                {
                                    dataGridCompare2.Rows[j].DefaultCellStyle.BackColor = Color.Yellow;
                                    dataGridCompare2.Rows[j].Cells[i].Style.BackColor = Color.LightCoral;
                                }
                                else
                                {
                                    higlighted = true;
                                    dataGridCompare2.Rows[j].DefaultCellStyle.BackColor = Color.Yellow;
                                    dataGridCompare2.Rows[j].Cells[i].Style.BackColor = Color.LightCoral;
                                }
                            }
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("NullRef on compare");
                        }

                    }
                }
                /**
                 * More rows on right than left, highlight in yellow
                 */
                if (rows_left > -1)
                {
                    for(int k = rows; k < cp2_row; k++){
                        dataGridCompare2.Rows[k].DefaultCellStyle.BackColor = Color.Yellow;
                    }
                }
            }
                //if columns do not match, show a message to the user!
            else
            {
                MessageBox.Show("Databases don't match based on their columns.\nCompare is not possible!");
                Console.WriteLine("Database column mismatch");
            }
        }

        /**
         * search(string query)
         * Search in a datagrid on the first tab
         * pre: table should be opened
         * post: highlighted rows
         */
        private void search(string query)
        {
            List<int> tab2 = new List<int>();

            //index the currently selected table
            if (checkBox1_SearchTables.Checked == false)
            {
                //get index of second tab, first lookup the name and then get the SelectedIndex from datagridview1
                for (int i = 0; i < ese_tablename_view.Rows.Count; i++)
                {
                    if (ese_tablename_view.Rows[i].Cells[1].FormattedValue.ToString().Equals(this.tabControl2.TabPages[this.tabControl2.SelectedIndex].Text))
                    {
                        tab2.Add(i);
                    }
                }
            }
            //loop trough all openTables and index them
            else if (checkBox1_SearchTables.Checked == true)
            {
                for (int i = 0; i < ese_tablename_view.Rows.Count; i++)
                {

                    for (int j = 0; j < this.tabControl2.TabCount; j++)
                    {
                        try
                        {
                            if (ese_tablename_view.Rows[i].Cells[1].FormattedValue.ToString().Equals(this.tabControl2.TabPages[j].Text))
                            {
                                tab2.Add(i);
                            }
                        }

                        catch (ArgumentOutOfRangeException)
                        {
                        }
                    }
                }
            }



            //go do the comparing part
            int matches = 0;
            int row_match = 0;
            int tmp_row = -1;
            int tbl_match = 0;
            int tmp_tbl = -1;
            Boolean last_match = false;
            try
            {
                int tab1 = this.tabControl1.SelectedIndex;

                for (int h = 0; h < tab2.Count; h++)
                {
                    // Set the selection background color for all the cells.
                    //ese_datatables[tab2[h]].DefaultCellStyle.BackColor = Color.White;
                    //ese_datatables[tab2[h]].DefaultCellStyle.ForeColor = Color.Black;

                    //loop through the table to find any match
                    for (int i = 0; i < ese_datatables[tab2[h]].Rows.Count; i++)
                    {
                        last_match = false;
                        for (int j = 0; j < ese_datatables[tab2[h]].Columns.Count; j++)
                        {
                            //ese_datatables[tab2[h]].Rows[i].DefaultCellStyle.BackColor = Color.White;
                            //ese_datatables[tab2[h]].Rows[i].Cells[j].Style.BackColor = Color.White;
                            //give rows back their white color when not match
                            if (highlight_lastsearch.Checked == false && last_match == false)
                            {
                                ese_datatables[tab2[h]].Rows[i].DefaultCellStyle.BackColor = Color.White;
                            }
                            //Console.WriteLine(dgv[tab2].Rows[i].Cells[j].FormattedValue.ToString());
                            if (ese_datatables[tab2[h]].Rows[i].Cells[j].FormattedValue.ToString().ToLower().Contains(query.ToLower()) && !query.Equals("") && checkBox1_Match.Checked == false)
                            {
                                last_match = true;
                                //keep track in how many rows and tables something was found
                                if (tmp_row != i)
                                {
                                    row_match++;
                                }
                                if(tmp_tbl != h){
                                    tbl_match++;
                                }
                                ese_datatables[tab2[h]].Rows[i].DefaultCellStyle.BackColor = Color.Yellow;
                                if (highlight_lastsearch.Checked == true)
                                {
                                    ese_datatables[tab2[h]].Rows[i].Cells[j].Style.BackColor = colors[keep_highlight_count];
                                }
                                else
                                {
                                    ese_datatables[tab2[h]].Rows[i].Cells[j].Style.BackColor = Color.Green;
                                    keep_highlight_count = 0;
                                }
                                matches++;
                                tmp_row = i;
                                tmp_tbl = h;
                            }
                            else if (ese_datatables[tab2[h]].Rows[i].Cells[j].FormattedValue.ToString().ToLower().Equals(query.ToLower()) && !query.Equals("") && checkBox1_Match.Checked == true)
                            {
                                //keep track in how many rows and tables something was found
                                if (tmp_row != i)
                                {
                                    row_match++;
                                }
                                if (tmp_tbl != h)
                                {
                                    tbl_match++;
                                }
                                ese_datatables[tab2[h]].Rows[i].DefaultCellStyle.BackColor = Color.Yellow;
                                ese_datatables[tab2[h]].Rows[i].Cells[j].Style.BackColor = Color.Green;
                                matches++;
                                tmp_row = i;
                                tmp_tbl = h;
                            }
                        }
                    }
                }
                keep_highlight_count++;
                if (keep_highlight_count == 6)
                {
                    keep_highlight_count = 0;
                }
            }
            catch (IndexOutOfRangeException)
            {

            }
            //show the amount of results to the user
            if(matches != 0 && tab2.Count == 1){
                searchResultsLabel.Text = "Result: "+matches+" match(es) in "+row_match+" row(s) in the current table";
            }
            else if (matches != 0 && tab2.Count > 1)
            {
                searchResultsLabel.Text = "Result: " + matches + " match(es) in " + tbl_match + " table(s) out of " + tab2.Count +" ";
            }
            else
            {
                searchResultsLabel.Text = "Result: 0 match(es) in the current table";
            }
        }

        /*
         *  ListBox to select a tablename to compare
         *  when user has clicked on a tablename in the Compare tabpage
         *  new datatable will be loaded and displayed.
         *  pre:
         *  post: new data source
         */
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String table = listBox1.SelectedItem.ToString();
            ese_datatable = new DataTable();
            ese_datatable = new EsentReader.esent.wlmReadTable(table, this.cmp1_file).QueryESE();
            dataGridCompare1.DataSource = ese_datatable;

            ese_datatable = new DataTable();
            ese_datatable = new EsentReader.esent.wlmReadTable(table, this.cmp2_file).QueryESE();
            dataGridCompare2.DataSource = ese_datatable;

            Console.WriteLine("Finished selecting new datasource");

            doCompare();
        }

       

        /**
         * Event called when the right side is being scrolled.
         * pre: left/right datagridview, scrollbars present
         * post: align left datagridview with right dgv.
         */
        private void Grid_Scrolled(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                try
                {
                    _horizontalOffsetStop = e.NewValue;
                    dataGridCompare1.HorizontalScrollingOffset = dataGridCompare2.HorizontalScrollingOffset;
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    //Console.WriteLine("VERTICAL SCROLL TO MUCH");
                }
            }
            else if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {
                try
                {
                    _horizontalOffsetStop = e.NewValue;
                    // dataGridCompare1.VerticalScrollingOffset = dataGridCompare2.VerticalScrollingOffset;
                    dataGridCompare1.FirstDisplayedScrollingRowIndex = dataGridCompare2.FirstDisplayedScrollingRowIndex;
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    //Console.WriteLine("VERTICAL SCROLL TO MUCH");
                }
            }
        }

        /**
         * Set horizontal offset of the slide on the left datagrid(Tab 2, Compare)
         * pre:
         * post:
         */
        private void Grid_Sorted(object sender, EventArgs e)
        {
            dataGridCompare1.HorizontalScrollingOffset = _horizontalOffsetStop;
        }



        /**
         * Removes all open tab pages in Tab 1
         * if tab is not opened it will still try
         * to do so. It throws an exception.
         * pre:
         * post: cleared all tabs on Tab 1
         */
        public void removeAllTabs()
        {
            try
            {
                for (int i = 0; i < this.tab_pages.Length; i++)
                {
                    try
                    {
                        tabControl2.TabPages.Remove(this.tab_pages[i]);
                    }
                    catch (System.ArgumentNullException)
                    {
                  
                        //Console.WriteLine(ane.Message);
                    }
                }
            }
            catch (System.NullReferenceException)
            {
            }
        }

        //ExportToCSV All Tables
        private void configuriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.filepath.Equals(""))
            {
                new eseExportToCsv().exportTables("", this.filepath, 1, ese_tablename_view);
                MessageBox.Show("Finished exporting all tables to CSV");
            }
        }

        //ExportToCSV Current Table
        private void exportToCSVCurrentTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.filepath.Equals(""))
            {
                try
                {
                    int dbPage = this.tabControl2.SelectedIndex;
                    string tblname = this.tabControl2.TabPages[dbPage].Text;
                    new eseExportToCsv().exportTables(tblname, this.filepath, 2, ese_tablename_view);
                    MessageBox.Show("Finished exporting " + tblname + " to CSV");
                }
                catch(System.ArgumentOutOfRangeException){
   
                }
            }            
        }

        //ExportToXML All Tables
        private void exportToXMLAllTablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.filepath.Equals(""))
            {
                new eseExportToXml().exportTables("", this.filepath, 1, ese_tablename_view);
                MessageBox.Show("Finished exporting all tables to XML");
            }
        }

        //ExportToXML Current Tables
        private void exportToXMLCurrentTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.filepath.Equals(""))
            {
                try{
                    int dbPage = this.tabControl2.SelectedIndex;
                    string tblname = this.tabControl2.TabPages[dbPage].Text;
                    new eseExportToXml().exportTables(tblname, this.filepath, 2, ese_tablename_view);
                    MessageBox.Show("Finished exporting " + tblname + " to XML");
                }
                catch(System.ArgumentOutOfRangeException){

                    
                }
            }   
        }

        //Export to CSV 
        private void button1_Click_1(object sender, EventArgs e)
        {
            //exportToFile(CSV);
        }
        //Export to CSV 
        private void button2_Click_1(object sender, EventArgs e)
        {

            exportToFile(CSV);
        }

        /**
         * Export to CSV or XML
         * Takes option as argument to define the export type: CSV or XML
         * post: saved export file on disk
         */
        private void exportToFile(int option)
        {
            if (!this.filepath.Equals(""))
            {
                try
                {
                    int dbPage1 = this.tabControl1.SelectedIndex;
                    int dbPage2 = this.tabControl2.SelectedIndex;
                    string tblname = "";
                    try
                    {
                        tblname = this.tabControl2.TabPages[dbPage2].Text;
                    }
                    catch (System.ArgumentOutOfRangeException)
                    {
                        tblname = "";
                    }

                    switch (option)
                    {
                        case CSV: 
                            switch(dbPage1){
                                case 0: new ExportToCSVForm(tblname, this.filepath, ese_tablename_view).ShowDialog(); break;
                                case 1: MessageBox.Show("These tables cannot be exported"); break;
                                case 2: this.wlm_categories_view.exportFolderView(CSV); break;
                            } break;
                        case XML:

                            switch (dbPage1)
                            {
                                case 0: new ExportXMLForm(tblname, this.filepath, ese_tablename_view).ShowDialog(); break;
                                case 1: MessageBox.Show("These tables cannot be exported"); break;
                                case 2: this.wlm_categories_view.exportFolderView(XML); break;
                            } break;
                    }

                }
                catch (System.ArgumentOutOfRangeException)
                {
                    //output
                }
            }
            else
            {
                MessageBox.Show("Open a file before trying to export");
            }
        }

        /**
         * Export WLM Categories to XML
         * pre: export categories to XML
         * post: XML file written to disk
         */
        private void exportContactTreeToXMLToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (!this.filepath.Equals(""))
            {
                this.wlm_categories_view.exportFolderView(XML);
            }
        }

        /**
         * Export WLM Categories to CSV
         * pre: export categories to CSV
         * post: CSV file written to disk
         */
        private void exportContactTreeToCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.filepath.Equals(""))
            {
                this.wlm_categories_view.exportFolderView(CSV);
            }
        }

        /**
         * Searchbox do search when 'enter' is pressed.
         * 
         */
        private void searchBox_Enter(object sender, EventArgs e)
        {
            if (!this.filepath.Equals(""))
            {
               // search(searchBox.Text);
            }
        }

        /**
         * Search when clicked on search-button
         * pre: query to search for
         * post: highlighted rows/cells with matched data
         */
        private void searchbutton_Click(object sender, EventArgs e)
        {
            if (!this.filepath.Equals(""))
            {
                search(searchBox.Text);
            }
        }

        private void licensToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new EsentReader.about.License().Show();
        }

    }
}

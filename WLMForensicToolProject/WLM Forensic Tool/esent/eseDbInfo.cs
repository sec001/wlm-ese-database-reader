﻿/*
  eseDbInfo.cs
	
  ESE database/table information: rows, columns, filesize, etc.
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: eseDbInfo.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EsentReader.esent
{
    class eseDbInfo
    {
        private string version;
        private const string WLM2009 = "SimpleContact-v081111-0856-1203";
        private const string WLM2011 = "SimpleContact-v100906-0137";

        public eseDbInfo()
        {
            //constructor
        }


        /**
         * Tries to determine if a version of WLM is loaded
         * by looking at a specific table
         * pre:     takes datagridview as argument
         * post:    version of WLM or empty string
         */
        public string getVersion(DataGridView datagrid)
        {
            this.version = "";
            for (int j = 0; j < datagrid.RowCount; j++)
            {
                if (datagrid[1, j].Value.Equals(WLM2009))
                {
                    this.version = "2009";
                }
                else if (datagrid[1, j].Value.Equals(WLM2011))
                {
                    this.version = "2011";
                }
            }
            return version;
        }

        /**
         * Tries to determine if a version of WLM is loaded
         * by looking at a specific table
         * pre:     takes datagridview as argument
         * post:    version of WLM or empty string
         */
        public string getVersion(List<string> table_names)
        {
            this.version = "";
            for (int j = 0; j < table_names.Count; j++)
            {
                if (table_names[j].Equals(WLM2009))
                {
                    this.version = "2009";
                }
                else if (table_names[j].Equals(WLM2011))
                {
                    this.version = "2011";
                }
            }
            //default: load first table
            return version;
        }

        /**
         * Get simplecontact row from list of tables
         * pre: List<string> table_names - list of all tables from database
         * post: return row of simplecontact or 0 if not found
         */
        public int getTableRowByList(List<string> table_names)
        {

            for (int j = 0; j < table_names.Count; j++)
            {
                if (table_names[j].Equals(WLM2009))
                {
                    return j;
                }
                else if (table_names[j].Equals(WLM2011))
                {
                    return j;
                }
            }
            //default: load first table
            return 0;
        }

        /**
         * Return the amount of tables(rows) in a datagridview
         * pre:     takes datagridview as argument
         * post:    amount of rows in a datatable
         */
        public int getTableCount(DataGridView datagrid)
        {
            return datagrid.RowCount;
        }

        /**
         * Return the file size of the DataBase in MB
         * pre:     filepath - path to file as string
         * post:    file length in MB
         */
        public long getFileSize(string filepath)
        {
            //get bytes as type long
            long fileSize = new System.IO.FileInfo(filepath).Length;
            //convert to KB
            fileSize = fileSize / 1024;
            //convert to MB
            fileSize = fileSize / 1024;

            return fileSize;
        }
    }
}

﻿/*
  wlmTreeView.cs
	
  Window Live Messenger TreeView(category) builder
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: wlmTreeView.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
namespace EsentReader.esent
{
    class wlmTreeView
    {
        private const string WLM2009 = "2009";
        private const string WLM2011 = "2011";

        public const int CSV = 0;
        public const int XML = 1;

        private string version;
        private string filename;
        private CommonTools.Node[] categoryNode;
        private DataTable categories, categorie_link, contacts, mecontact;
        private DataTable export_tbl;

        //columns
        int msnEA, homeEA, otherEA, nickname, status_msg, title, fname, mname = -1;
        int lname, flname, gender, nickn, bday, pf_lc, pf_ls, wl_cd, wl_lc, wl_purl = -1;
        int contactsRowId = -1;

        public wlmTreeView(string version, string filename)
        {
            this.version = version;
            this.filename = filename;
            if (this.version.Equals(WLM2009))
            {
                categories = getDataTable("GroupView-v081111-0856-1203");
                categorie_link = getDataTable("Group-v081111-0856-1203");
                contacts = getDataTable("SimpleContact-v081111-0856-1203");
                //  mecontact = getDataTable("MeContact-v100906-0137");
            }
            else if (this.version.Equals(WLM2011))
            {
                categories = getDataTable("Category-v100906-0137");
                categorie_link = getDataTable("CategoryLink-v100906-0137");
                contacts = getDataTable("SimpleContact-v100906-0137");
                mecontact = getDataTable("MeContact-v100906-0137");

                pf_lc = getColumnID(contacts, "ProfileLastChanged");
                pf_ls = getColumnID(contacts, "ProfileLastSynced");
            }
           
            //GET COLUMN IDs of the fields we want to display
            contactsRowId = getColumnID(contacts, "RowId");
            msnEA = getColumnID(contacts, "MsnAddress");
            otherEA = getColumnID(contacts, "OtherIMEmail");
            homeEA = getColumnID(contacts, "HomeEmail");
            nickname = getColumnID(contacts, "FriendlyName");
            title = getColumnID(contacts, "Title");
            fname = getColumnID(contacts, "FirstName");
            mname = getColumnID(contacts, "MiddleName");
            lname = getColumnID(contacts, "LastName");
            flname = getColumnID(contacts, "FriendlyName");
            nickn = getColumnID(contacts, "NickName");
            gender = getColumnID(contacts, "Gender");
            bday = getColumnID(contacts, "Birthdate");
            
            wl_cd = getColumnID(contacts, "WL_CreateDate");
            wl_lc = getColumnID(contacts, "WL_LastChanged");
            wl_purl = getColumnID(contacts, "WL_ProfileURL");

            this.categoryNode = new CommonTools.Node[categories.Rows.Count + 3];
        }

        /**
         * Create a datatable to be used for export
         * pre: option to export CSV/XML
         * post: export of WLM Contact Categories
         */
        public void createExportTable(int option)
        {
            export_tbl = new DataTable();
            export_tbl.Columns.Add("Category", typeof(string));
            export_tbl.Columns.Add("Email", typeof(string));
            export_tbl.Columns.Add("Status", typeof(string));
            export_tbl.Columns.Add("Emailaddress", typeof(string));
            export_tbl.Columns.Add("NickName", typeof(string));
            export_tbl.Columns.Add("StatusMsg", typeof(string));
            export_tbl.Columns.Add("Title", typeof(string));
            export_tbl.Columns.Add("Firstname", typeof(string));
            export_tbl.Columns.Add("Middlename", typeof(string));
            export_tbl.Columns.Add("Lastname", typeof(string));
            export_tbl.Columns.Add("Friendlyname", typeof(string));
            export_tbl.Columns.Add("Gender", typeof(string));
            export_tbl.Columns.Add("BirthDate", typeof(string));
            export_tbl.Columns.Add("MSN", typeof(string));
            export_tbl.Columns.Add("Home", typeof(string));
            export_tbl.Columns.Add("Other", typeof(string));
            export_tbl.Columns.Add("ProfileLastChanged", typeof(string));
            export_tbl.Columns.Add("ProfileLastSynced", typeof(string));
            export_tbl.Columns.Add("WL_CreateDate", typeof(string));
            export_tbl.Columns.Add("WL_LastChanged", typeof(string));
            export_tbl.Columns.Add("WL_ProfileURL", typeof(string));

            if (!this.version.Equals(WLM2009))
            {
                //first get all contacts which are in a category
                for (int i = 0; i < categorie_link.Rows.Count; i++)
                {
                    int j = getCategoryLinkContactId(i);
                    CommonTools.Node node = getContactInfo(j);
                    export_tbl.Rows.Add(this.categoryNode[getCategoryId(j, i)][0].ToString(), "" + node[0].ToString(), "" + node[1].ToString(), "" + node[2].ToString(), "" + node[3].ToString(), "" + node[4].ToString(), "" + node[5].ToString(), "" + node[6].ToString(), "" + node[7].ToString(), "" + node[8].ToString(), "" + node[9].ToString(), "" + node[10].ToString(), "" + node[11].ToString(), "" + node[12].ToString(), "" + node[13].ToString(), "" + node[14].ToString(), "" + node[15].ToString(), "" + node[16].ToString(), "" + node[17].ToString(), "" + node[18].ToString(), "" + node[19].ToString());

                }

                //if already added to category don't add to other contacts
                //if not added before add to other contacts
                for (int i = 0; i < contacts.Rows.Count; i++)
                {
                    if (isInCategory(i) != -1)
                    {
                        CommonTools.Node node = getContactInfo(i);
                        export_tbl.Rows.Add(this.categoryNode[getCategoryId(i, -1)][0].ToString(), "" + node[0].ToString(), "" + node[1].ToString(), "" + node[2].ToString(), "" + node[3].ToString(), "" + node[4].ToString(), "" + node[5].ToString(), "" + node[6].ToString(), "" + node[7].ToString(), "" + node[8].ToString(), "" + node[9].ToString(), "" + node[10].ToString(), "" + node[11].ToString(), "" + node[12].ToString(), "" + node[13].ToString(), "" + node[14].ToString(), "" + node[15].ToString(), "" + node[16].ToString(), "" + node[17].ToString(), "" + node[18].ToString(), "" + node[19].ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show("There is a problem exporting WLM2009. This is a known issue and is being worked on!\nSorry for the inconvience.");
            }

            if(this.version.Equals(WLM2011)){
                //Add user-info to datatable
                CommonTools.Node node2 = getUserInfo();
                export_tbl.Rows.Add("User Info", "" + node2[0].ToString(), "" + node2[1].ToString(), "" + node2[2].ToString(), "" + node2[3].ToString(), "" + node2[4].ToString(), "" + node2[5].ToString(), "" + node2[6].ToString(), "" + node2[7].ToString(), "" + node2[8].ToString(), "" + node2[9].ToString(), "" + node2[10].ToString(), "" + node2[11].ToString(), "" + node2[12].ToString(), "" + node2[13].ToString(), "" + node2[14].ToString(), "" + node2[15].ToString(), "" + node2[16].ToString(), "" + node2[17].ToString(), "" + node2[18].ToString(), "" + node2[19].ToString());
            }

            //export to CSV or XML
            switch (option)
            {
                case CSV:
                    new eseExportToCsv().exportContactTreeView(export_tbl);
                    MessageBox.Show("Finished exporting Contact Categories to CSV");
                    break;
                case XML:
                    new eseExportToXml().treeViewExport("ContactCategories_-_" + DateTime.Now.ToString("MM-dd-yyyy_HHmmss") + ".xml", export_tbl, this.categoryNode);
                    break;
            }
        }



        /*
         * Return all categories as Node array
         * pre: -
         * post: -
         */
        public CommonTools.Node[] getCategories()
        {
            //loop through all categories
            for (int i = 0; i < categories.Rows.Count; i++)
            {
                this.categoryNode[i] = new CommonTools.Node();
                if (this.version.Equals(WLM2009))
                {
                    this.categoryNode[i][0] = categories.Rows[i][getColumnID(categories,"Name")].ToString();
                }
                else
                {
                    this.categoryNode[i][0] = categories.Rows[i][8].ToString();
                    //Console.WriteLine(categories.Rows[i][8].ToString());
                }
            }
            //Other mecontact(not in category)
            this.categoryNode[(categories.Rows.Count + 1)] = new CommonTools.Node();
            this.categoryNode[(categories.Rows.Count + 1)][0] = "Other contacts";

            //only WLM 2011 has a table containing specific user information
            if (this.version.Equals(WLM2011))
            {
                //User information
                this.categoryNode[(categories.Rows.Count + 2)] = new CommonTools.Node();
                this.categoryNode[(categories.Rows.Count + 2)][0] = "User Info";
            }

            return categoryNode;
        }


        /**
         * Get Category ID
         * Get the categories you belong to
         * pre: contact(rowid), category
         * post: return int of the category
         */
        public int getCategoryId(int contact, int category)
        {
            if(this.version.Equals(WLM2009)){
                //contacts category in WLM2009 has to be looked at. 
                //uses a different way
            }
            else{
                //-1 you are not in a category (other contacts is where you belong)
                if (category != -1)
                {
                    //find if you belong to a category
                    if (contacts.Rows[contact][contactsRowId].ToString().Equals(categorie_link.Rows[category][1].ToString()))
                    {
                        for (int m = 0; m < (categories.Rows.Count); m++)
                        {
                            //find which category
                            if (categorie_link.Rows[category][0].ToString().Equals(categories.Rows[m][13]))
                            {
                                return m;
                            }

                        }
                    }
                }
            }
            //IF NOT FOUND RETURN +1 is OTHER CONTACTS
            return (categories.Rows.Count + 1);
        }

        /**
         * isInCategory()
         * Determines if you are in a category 
         * pre: id(contactid)
         * post: if you are in a category return -1. If you are not return 0.
         */
        public int isInCategory(int id){
            for (int i = 0; i < categorie_link.Rows.Count; i++)
            {
                if (contacts.Rows[id][contactsRowId].ToString().Equals(categorie_link.Rows[i][1].ToString()))
                {
                    return -1;
                }
            }
            return 0;
        }


        /**
         * Get ContactId from category_link table
         * Uses: search in SimpleContacts to get ContactInfo
         * return row in SimpleContact
         */
        public int getCategoryLinkContactId(int id)
        {
            int result = -1;
            if(version.Equals("2011")){
                string catlinkContactId = categorie_link.Rows[id][1].ToString();
                for (int i = 0; i < contacts.Rows.Count; i++)
                {
                    if (contacts.Rows[i][contactsRowId].ToString().Equals(categorie_link.Rows[id][1].ToString()))
                    {
                        return i;
                    }
                }
            }
            return result;
        }


        /**
         *  Return UserInfo (email, name, nick, etc) 
         */
        public CommonTools.Node getUserInfo()
        {
            int row = 0;
            CommonTools.Node node = new CommonTools.Node();

            //get columns based on version
            //2009/2011 uses a different columns
            if (this.version.Equals(WLM2009))
            {
                node[(int)eColumns.Email] = "";
                // node[(int)eColumns.Status] = mecontact.Rows[row][getColumnID(mecontact,"QuickName")].ToString();
                node[(int)eColumns.NickName] = "";
                //node[(int)eColumns.Message] = mecontact.Rows[row][getColumnID(mecontact, "")].ToString();
                node[(int)eColumns.MSN] = "";
                node[(int)eColumns.Status] = "";


                //node[(int)eColumns.Title] = mecontact.Rows[row][getColumnID(mecontact, "")].ToString();
                node[(int)eColumns.FirstName] = "";
                //node[(int)eColumns.MiddleName] = mecontact.Rows[row][getColumnID(mecontact, "")].ToString();
                node[(int)eColumns.LastName] = "";
                node[(int)eColumns.FriendlyName] = "";
                node[(int)eColumns.Gender] = "";
                // node[(int)eColumns.NickName2] = mecontact.Rows[row][nickname].ToString();
                //node[(int)eColumns.BirthDate] = mecontact.Rows[row][getColumnID(mecontact, "")].ToString();

                node[(int)eColumns.Msn] = "";
                node[(int)eColumns.Home] = "";
                node[(int)eColumns.Other] = "";


                //node[(int)eColumns.ProfLC] = mecontact.Rows[row][getColumnID(mecontact, "")].ToString();
                //node[(int)eColumns.ProfLS] = mecontact.Rows[row][getColumnID(mecontact, "")].ToString();
                //node[(int)eColumns.WL_CDate] = mecontact.Rows[row][getColumnID(mecontact, "")].ToString();
                node[(int)eColumns.WL_LChanged] = "";
                node[(int)eColumns.WL_ProfURL] = "";
                //node[(int)eColumns.Other] = mecontact.Rows[row][otherEA].ToString();         

            }
            else if (this.version.Equals(WLM2011))
            {
                
                node[(int)eColumns.Email] = mecontact.Rows[row][getColumnID(mecontact, "MsnAddress")].ToString();
                // node[(int)eColumns.Status] = mecontact.Rows[row][getColumnID(mecontact,"QuickName")].ToString();
                node[(int)eColumns.NickName] = mecontact.Rows[row][getColumnID(mecontact, "PublicDisplayName")].ToString();
                node[(int)eColumns.Message] = "";
                node[(int)eColumns.MSN] = "";
                node[(int)eColumns.Status] = "";

                node[(int)eColumns.Title] = "";
                node[(int)eColumns.FirstName] = mecontact.Rows[row][getColumnID(mecontact, "FirstName")].ToString();
                node[(int)eColumns.MiddleName] = "";
                node[(int)eColumns.LastName] = mecontact.Rows[row][getColumnID(mecontact, "FullName")].ToString();
                node[(int)eColumns.FriendlyName] = mecontact.Rows[row][getColumnID(mecontact, "FriendlyName")].ToString();
                node[(int)eColumns.Gender] = mecontact.Rows[row][getColumnID(mecontact, "Gender")].ToString();
                // node[(int)eColumns.NickName2] = mecontact.Rows[row][nickname].ToString();
                node[(int)eColumns.BirthDate] = "";

                node[(int)eColumns.Msn] = mecontact.Rows[row][getColumnID(mecontact, "MsnAddress")].ToString();
                node[(int)eColumns.Home] = mecontact.Rows[row][getColumnID(mecontact, "HomeEmail")].ToString();
                node[(int)eColumns.Other] = mecontact.Rows[row][getColumnID(mecontact, "OtherEmail")].ToString();


                node[(int)eColumns.ProfLC] = "";
                node[(int)eColumns.ProfLS] = "";
                node[(int)eColumns.WL_CDate] = "";
                node[(int)eColumns.WL_LChanged] = mecontact.Rows[row][getColumnID(mecontact, "WL_LastChanged")].ToString();
                node[(int)eColumns.WL_ProfURL] = mecontact.Rows[row][getColumnID(mecontact, "WL_ProfileURL")].ToString();
                //node[(int)eColumns.Other] = mecontact.Rows[row][otherEA].ToString();         

            }
            return node;
        }


        /**
         *  Return ContactInfo (email, name, nick, etc) 
         *  Get all needed information from a contact
         *  pre: int row(contact rowid)
         *  post: Node
         */
        public CommonTools.Node getContactInfo(int row)
        {
            //int row = 0;
            CommonTools.Node node = new CommonTools.Node();

            node[(int)eColumns.Email] = getContactEmailaddress(contacts, row);
            // node[(int)eColumns.Status] = contacts.Rows[row][getColumnID(contacts,"QuickName")].ToString();
            node[(int)eColumns.NickName] = contacts.Rows[row][nickname].ToString();
            node[(int)eColumns.Message] = contacts.Rows[row][status_msg].ToString();
            node[(int)eColumns.MSN] = "";
            node[(int)eColumns.Status] = "";
            node[(int)eColumns.Title] = contacts.Rows[row][title].ToString();
            node[(int)eColumns.FirstName] = contacts.Rows[row][fname].ToString();
            node[(int)eColumns.MiddleName] = contacts.Rows[row][mname].ToString();
            node[(int)eColumns.LastName] = contacts.Rows[row][lname].ToString();
            node[(int)eColumns.FriendlyName] = contacts.Rows[row][flname].ToString();
            node[(int)eColumns.Gender] = contacts.Rows[row][gender].ToString();
            // node[(int)eColumns.NickName2] = contacts.Rows[row][nickname].ToString();
            node[(int)eColumns.BirthDate] = contacts.Rows[row][bday].ToString();

            node[(int)eColumns.Msn] = contacts.Rows[row][msnEA].ToString();
            node[(int)eColumns.Home] = contacts.Rows[row][homeEA].ToString();
            node[(int)eColumns.Other] = contacts.Rows[row][otherEA].ToString();

            node[(int)eColumns.WL_CDate] = contacts.Rows[row][wl_cd].ToString();
            node[(int)eColumns.WL_LChanged] = contacts.Rows[row][wl_lc].ToString();
            node[(int)eColumns.WL_ProfURL] = contacts.Rows[row][wl_purl].ToString(); 

            //WLM2009 doesn't have the following two columns
            if (this.version.Equals(WLM2009))
            {
                node[(int)eColumns.ProfLC] = "";
                node[(int)eColumns.ProfLS] = "";
            }
            else if (this.version.Equals(WLM2011))
            {
                node[(int)eColumns.ProfLC] = contacts.Rows[row][pf_lc].ToString();
                node[(int)eColumns.ProfLS] = contacts.Rows[row][pf_ls].ToString();
            }
            return node;
        }


        /**
         * Get table for processing
         * return datatable
         */
        public DataTable getDataTable(string tablename)
        {
           return new EsentReader.esent.wlmReadTable(tablename, this.filename).QueryESE();
        }

        /**
         * Get amount of user contacts
         * return int
         */
        public int getContactCount()
        {
            return contacts.Rows.Count;
        }

        /**
         * Return amount of categories
         */
        public int getCategoryCount()
        {
            return categories.Rows.Count;
        }

        /**
         * Return amount of category links
         * links map contact to category
         */
        public int getCategoryLinkCount()
        {
            return categorie_link.Rows.Count;
        }


        /**
         * Get ColumnId based on ColumnName if found return value
         * else return -1.
         */
        private int getColumnID(DataTable db, string column)
        {
            for (int j = 0; j < db.Columns.Count; j++)
            {
                if (db.Columns[j].ColumnName.ToString().Equals(column))
                {
                    return j;
                }
            }
            return -1;
        }


        /**
         * Check which of the emailaddresses is available, break when one
         * is found or return '---' if none is found.
         */
        private string getContactEmailaddress(DataTable db, int i)
        {
            try
            {
                if (!db.Rows[i][msnEA].ToString().Equals(""))
                {
                    return db.Rows[i][msnEA].ToString();
                }
                else if (!db.Rows[i][otherEA].ToString().Equals(""))
                {
                    return db.Rows[i][otherEA].ToString();
                }
                else if (!db.Rows[i][homeEA].ToString().Equals(""))
                {
                    return db.Rows[i][homeEA].ToString();
                }
                else
                {
                    return "---";
                }
            }
            catch (IndexOutOfRangeException)
            {
                return "---";
            }
        }

        /***
         *  Enumeration of columns in ContactCategoryView
         */
        private enum eColumns
        {
            // this must match the order at which the columns are added
            Email = 0,
            Status = 1,
            MSN = 2,
            NickName = 3,
            Message = 4,
            Title = 5,
            FirstName = 6,
            MiddleName = 7,
            LastName = 8,
            FriendlyName = 9,
            Gender = 10,
            BirthDate = 11,
            Msn = 12,
            Home = 13,
            Other = 14,
            ProfLC = 15,
            ProfLS = 16,
            WL_CDate = 17,
            WL_LChanged = 18,
            WL_ProfURL = 19
        }

    }
}

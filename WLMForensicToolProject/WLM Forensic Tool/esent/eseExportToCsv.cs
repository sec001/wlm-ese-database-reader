﻿/*
  eseExportToCsv.cs
	
  Export data to CSV file format
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: eseExportToCsv.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Windows.Forms;
using Microsoft.Isam.Esent.Interop;
using EsentReader.esent;
using System.IO;

namespace EsentReader.esent
{
    class eseExportToCsv
    {
        //constants
        public const int ALL_TO_ONE = 0;
        public const int ALL_TO_MUL = 1;
        public const int CUR_TABLE  = 2;
        public const int WLM_CONTACTS = 3;


        private string fileName, old_filename;
        private string tableName;
        private DataGridView datagrid;
        private DataTable dataTable;
        private Boolean exportSucces;

        public eseExportToCsv() //database name or location, table, maybe database pagesize
        {
            Console.WriteLine("Loading XML export Constructor");    
        }

        /**
         * exportTables()
         * 
         * pre: tablename, filename, option(what to export), datagrid(table with data)
         * post: return exportSucces (true/false)
         */
        public Boolean exportTables(string tablename, string filename, int option, DataGridView dgv)
        {
            Console.WriteLine("Start exporting table(s) to CSV file");
            this.fileName       = filename;
            this.tableName      = tablename;
            //this.curOpenTables  = curOpenTables;
            this.datagrid       = dgv;
            this.exportSucces   = false;

            this.exportSucces = export(option);

            Console.WriteLine("Finished exporting table(s) to CSV");
            return this.exportSucces;
        }

        /**
         * exportContactTreeView(DataTable contacts)
         * Init - exportContactTreeView
         * prepares Contact Categories to be exported to 
         * CSV
         * pre: requires datatable of all the contacts
         * post: result of export(true/false)
         */
        public Boolean exportContactTreeView(DataTable contacts)
        {
            this.fileName = "";
            this.tableName = "ContactCategories";
            this.dataTable = contacts;
            this.exportSucces = false;
            this.exportSucces = export(WLM_CONTACTS);

            Console.WriteLine("Finished exporting ContactCategories to CSV");

            return this.exportSucces;
        }

        /**
         * export(int option)
         * depening on your choice it will select the appropiate 
         * export option.
         * pre: option to export
         * post: result of export(true/false)
         */
        private Boolean export(int option)
        {
            this.old_filename = this.fileName;
            Boolean export = false;

            switch (option)
            {
                case ALL_TO_MUL:
                    for (int i = 0; i < this.datagrid.Rows.Count; i++)
                    {
                        this.tableName = this.datagrid.Rows[i].Cells[1].FormattedValue.ToString();
                        this.fileName = GenFilename(option);
                        this.dataTable = new wlmReadTable(this.tableName, this.old_filename).QueryESE();
                        this.dataTable.TableName = this.tableName;

                        exportCSV();                     
                    }
                    break;
                case CUR_TABLE:
                    this.fileName = GenFilename(option);
                    this.dataTable = new wlmReadTable(this.tableName, this.old_filename).QueryESE();
                    this.dataTable.TableName = this.tableName;

                    exportCSV();
                    break;

                case WLM_CONTACTS:
                    this.fileName = GenFilename(option);
                    exportCSV();
                 break;

            }
            return export;
        }      

        /**
         * exportCSV()
         * export an ESE datatable to CSV
         * pre: ese datatable
         * post: CSV file written to disk
         */
        private void exportCSV()
        {
            var sw = new StreamWriter(this.fileName, false);

            // Write the headers.
            int iColCount = this.dataTable.Columns.Count;
            for (int j = 0; j < iColCount; j++)
            {
                sw.Write(this.dataTable.Columns[j]);
                if (j < iColCount - 1) sw.Write(";");
            }

            sw.Write(sw.NewLine);

            // Write rows.
            foreach (DataRow dr in this.dataTable.Rows)
            {
                for (int k = 0; k < iColCount; k++)
                {
                    if (!Convert.IsDBNull(dr[k]))
                    {
                        if (dr[k].ToString().StartsWith("0"))
                        {
                            sw.Write(@"=""" + dr[k] + @"""");
                        }
                        else
                        {
                            sw.Write(dr[k].ToString());
                        }
                    }

                    if (k < iColCount - 1) sw.Write(";");
                }
                sw.Write(sw.NewLine);
            }

            sw.Close();
        }

        /**
         * GenFilename(int option)
         * Generates a filename for the exported file.
         * uses the file- or tablename. Also date and time
         * are used to prevent overwriting of existing files
         * pre:
         * post: returns a filename to be used for exporting
         */
        private string GenFilename(int option)
        {
            string tmp_fn = System.IO.Path.GetFileName(this.old_filename).ToString();
            string time = DateTime.Now.ToString("MM-dd-yyyy_HHmmss");
            string new_fn = "";

            switch (option)
            {
                case ALL_TO_ONE:
                    new_fn = tmp_fn;
                    break;
                case ALL_TO_MUL:
                    new_fn = tmp_fn + "_" + this.tableName;
                    break;
                case CUR_TABLE:
                    new_fn = tmp_fn + "_" + this.tableName;
                    break;
                case WLM_CONTACTS:
                    new_fn = "ContactCategories";
                    break;
                default: new_fn = "export_ese";
                    break;
            }

            return new_fn = new_fn + "_-_" + time + ".csv";
        }
    } 
}
﻿/*
  eseExportToXml.cs
	
  Export data to XML file format
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: eseExportToXml.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Windows.Forms;
using Microsoft.Isam.Esent.Interop;
using EsentReader.esent;

namespace EsentReader.esent
{
    class eseExportToXml
    {
        //const
        public const int ALL_TO_ONE = 0;
        public const int ALL_TO_MUL = 1;
        public const int CUR_TABLE  = 2;

        private string fileName,old_filename;
        private string tableName;
        private DataGridView datagrid;
        private DataTable dataTable;
        private Boolean exportSucces = false;

        /// <summary>
        /// Called on program startup.
        /// </summary>
        /// CONTSTRUCTOR
        public eseExportToXml() //database name or location, table, maybe database pagesize
        {
            Console.WriteLine("Loading XML export Constructor");    
        }

         /**
         * exportTables()
         * 
         * pre: tablename, filename, option(what to export), datagrid(table with data)
         * post: return exportSucces (true/false)
         */
        public Boolean exportTables(string tablename, string filename, int option, DataGridView dgv)
        {
            Console.WriteLine("Start exporting table(s) to XML file");
            this.fileName       = filename;
            this.tableName      = tablename;
            this.datagrid            = dgv;
            this.exportSucces   = false;

            this.exportSucces = export(option);

            Console.WriteLine("Finished exporting table(s) to XML");
            return this.exportSucces;
        }

        /**
         * export(int option)
         * depening on your choice it will select the appropiate 
         * export option.
         * pre: option to export
         * post: result of export(true/false)
         */
        private Boolean export(int option)
        {
            this.old_filename = this.fileName;
            Boolean export = false;

            switch (option)
            {
                case ALL_TO_MUL:
                    //export all
                    for (int i = 0; i < this.datagrid.Rows.Count; i++)
                    {
                        this.tableName = this.datagrid.Rows[i].Cells[1].FormattedValue.ToString();
                        this.fileName = GenFilename(option);
                        this.dataTable = new wlmReadTable(this.tableName, this.old_filename).QueryESE();
                        this.dataTable.TableName = this.tableName;

                        this.dataTable.WriteXml(this.fileName, true);
                    }
                    break;
                    //export current table
                case CUR_TABLE:
                    this.fileName = GenFilename(option);
                    this.dataTable = new wlmReadTable(this.tableName, this.old_filename).QueryESE();
                    this.dataTable.TableName = this.tableName;

                    this.dataTable.WriteXml(this.fileName, true);

                    break;
            }      
            return export;
        }

        /**
         * exportContactTreeView(DataTable contacts)
         * Init - exportContactTreeView
         * prepares Contact Categories to be exported to 
         * CSV
         * pre: requires datatable of all the contacts
         * post: result of export(true/false)
         */
        public void treeViewExport(string filename, DataTable contacts,CommonTools.Node[] categories)
        {
            XmlTextWriter w = new XmlTextWriter(filename, null);
            w.Formatting = Formatting.Indented;
            w.WriteStartElement("contacts");
            
            for (int i = 0; i < categories.Length; i++)
            {
                try
                {
                    //ROOT ELEMENT
                    if (!categories[i][0].Equals(""))
                    {
                        w.WriteStartElement("category");

                        w.WriteAttributeString("name", categories[i][0].ToString());

                        for (int j = 0; j < contacts.Rows.Count; j++)
                        {
                            if (contacts.Rows[j][0].ToString().Equals(categories[i][0].ToString()))
                            {
                                w.WriteStartElement("person");
                                //WRITE COLUMNNAME AS XML-ELEMENT
                                for (int k = 1; k < 19; k++)
                                {
                                    w.WriteStartElement(contacts.Columns[k].ToString());
                                    w.WriteString(contacts.Rows[j][k].ToString());
                                    w.WriteEndElement();
                                }
                                w.WriteEndElement();
                            }
                        }
                    }
                    //END RECORD XML-ELEMENT(PARENT) of COLUMNNAME
                    w.WriteEndElement();
                }
                catch(NullReferenceException){
                    Console.WriteLine("Instance" + i);
                }                    
                }
                //CLOSE ROOT ELEMENT (TABLENAME)
            //CLOSE XMLWRITER
            w.WriteEndElement();
            w.Close();
            MessageBox.Show("Finished exporting Contact Categories to XML");
        }


        /**
         * GenFilename(int option)
         * Generates a filename for the exported file.
         * uses the file- or tablename. Also date and time
         * are used to prevent overwriting of existing files
         * pre:
         * post: returns a filename to be used for exporting
         */
        private string GenFilename(int option)
        {
            string tmp_fn = System.IO.Path.GetFileName(this.old_filename).ToString();
            string time = DateTime.Now.ToString("MM-dd-yyyy_HHmmss");
            string new_fn = "";

            switch (option)
            {
                case ALL_TO_ONE:
                    new_fn = tmp_fn;
                    break;
                case ALL_TO_MUL:
                    new_fn = tmp_fn + "_" + this.tableName;
                    break;
                case CUR_TABLE:
                    new_fn = tmp_fn+"_"+this.tableName;
                    break;
                default: new_fn = "export_ese";  
                    break;
            }

           return new_fn = new_fn + "_-_" + time + ".xml";
        }
    } 
}

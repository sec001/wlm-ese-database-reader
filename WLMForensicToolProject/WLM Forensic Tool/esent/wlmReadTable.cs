﻿/*
  wlmReadTable.cs
	
  Open and read ESE database files
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: wlmReadTable.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using Microsoft.Isam.Esent.Interop;
using Microsoft.Isam.Esent.Interop.Vista;
using Microsoft.Isam.Esent.Interop.Windows7;
using Microsoft.Isam.Esent.Interop.Win32;
namespace EsentReader.esent
{
    class wlmReadTable
    {
        private string TableName;
        private DataTable dataTable;
        private DataColumn[] column;
        private DataRow row;
        
        /**
         * OpenDatabase() types
         */
        private string recovery;
        private List<string> tables = new List<string>();

        //ESE types
        private static JET_COLUMNID columnid;
        private static JET_COLUMNID[] columnsid;
        IDictionary<string, JET_COLUMNID> columnids;
        private static string[] columnNames;
        private static JET_DBID dbid;
        private string DatabaseName;
        private static JET_COLUMNDEF columndef;
        private static JET_COLUMNBASE columndefVista;

        //Constructor
        public wlmReadTable(String tablename, String filename) //database name or location, table, maybe database pagesize
        {
            dataTable = new DataTable();
            this.TableName = tablename;
            this.DatabaseName = filename;
        }

        /**
         * QueryESE()
         * Query an ESE database table. The tablename provided in the constructor
         * will be used as source for the query. It will read the complete table
         * and stores it in a datatable. The datatable will be returned.
         * pre: tablename, filename must be set in the constructor
         * post: return a datatable
         */
        public DataTable QueryESE()
        {
            int dbPageSize;
            Console.WriteLine("Read DatabasePageSize");
            Api.JetGetDatabaseFileInfo(this.DatabaseName, out dbPageSize, JET_DbInfo.PageSize);
            Console.WriteLine("DatabasePageSize:[" + dbPageSize + "]");

            SystemParameters.DatabasePageSize = dbPageSize;
            // Now the database has been created we can attach to it.
            // The Instance object we create is disposable, so the underlying ESENT
            // resource can be freed. The disposable objects wrapped around ESENT resources
            // _must_ be disposed properly. ESENT resources must be freed in a
            // specific order, which the finalizer doesn't necessarily respect.
            using (var instance = new Instance("ese_examination"))
            {
                // Creating an Instance object doesn't call JetInit. 
                // This is done to allow some parameters to be set
                // before the instance is initialized.
                Api.JetSetSystemParameter(instance, JET_SESID.Nil, JET_param.Recovery, 0, "off");
                // Circular logging is very useful; it automatically deletes
                // logfiles that are no longer needed. Most applications
                // should use it.
                instance.Parameters.CircularLog = true;
               // instance.Parameters.
                // Initialize the instance. This creates the logs and temporary database.
                // If logs are present in the log directory then recovery will run
                // automatically.
                instance.Init();

                // Create a disposable wrapper around a new JET_SESID. All database
                // access is done with a session (JET_SESID). Transactions and database
                // record visibility is per-session. Do not share sessions between
                // threads, instead create different sessions for different threads.
                using (var session = new Session(instance))
                {
                    

                    // The database only has to be attached once per instance, but each
                    // session has to open the database. Redundant JetAttachDatabase calls
                    // are safe to make though.
                    // Here we use the fact that Instance, Session, and Table objects all have
                    // implicit conversions to the underlying JET_* types. This allows the
                    // disposable wrappers to be used with any APIs that expect the JET_*
                    // structures.
                    //Attach and OpenDatabase are set to readonly. This ensures we cannot manipulate the 
                    //ESE database file
                    Api.JetAttachDatabase(session, this.DatabaseName, AttachDatabaseGrbit.ReadOnly);
                    Api.JetOpenDatabase(session, this.DatabaseName, null, out dbid, OpenDatabaseGrbit.ReadOnly);

                    // Create a disposable wrapper around the JET_TABLEID.
                    // A JET_TABLEID acts as a database cursor, it can be used to:
                    //  - Seek to a record
                    //  - Retrieve columns from a record
                    //  - Insert/Update/Delete a record
                    //  - Move to the next/previous record
                    using (var table = new Table(session, dbid, this.TableName, OpenTableGrbit.ReadOnly))
                    {
                        // Load the columnids from the table. This should be done each time the database is attached
                        // because an offline defrag (esentutl /d) can change the name => columnid mapping.
                        columnids = Api.GetColumnDictionary(session, table);
                        
                        columnsid = new JET_COLUMNID[columnids.Count];
                        Console.WriteLine("Amount of Columns:" + columnids.Count);
                        columnNames = new string[columnids.Count];
                        column = new DataColumn[columnids.Count];

                        //Console.WriteLine("Columns: "+columnids.Count);
                        int i = 0;
                        foreach (var pair in columnids)
                        {
                            columnsid[i] = pair.Value;
                            columnNames[i] = pair.Key;

                            column[i] = new DataColumn();
                            column[i].DataType = System.Type.GetType("System.String");
                            column[i].ColumnName = pair.Key;
                            column[i].ReadOnly = true;
                            column[i].Unique = false;
                            // Add the Column to the DataColumnCollection.
                            dataTable.Columns.Add(column[i]);
                            i++;
                        }
                        //DUMP ALL columnsname into table
                        // Dump records by the primary index;
                        Console.WriteLine("** DumpByIndex[ "+ TableName +"]");
                        DumpByIndex(session, table, null);
                    }
                }
            }

            return dataTable;
        }

        /// <summary>
        /// Dump all records from the index.
        /// </summary>
        /// <param name="sesid">The session to use.</param>
        /// <param name="tableid">The table to dump.</param>
        /// <param name="index">The index to use.</param>
        private void DumpByIndex(JET_SESID sesid, JET_TABLEID tableid, string index)
        {
            Api.JetSetCurrentIndex(sesid, tableid, index);
            PrintAllRecords(sesid, tableid);
            Console.WriteLine();
        }

        /// <summary>
        /// Print all rows in the table.
        /// </summary>
        /// <param name="sesid">The session to use.</param>
        /// <param name="tableid">The table to dump the records from.</param>
        private void PrintAllRecords(JET_SESID sesid, JET_TABLEID tableid)
        {
            if (Api.TryMoveFirst(sesid, tableid))
            {
                PrintRecordsToEnd(sesid, tableid);
            }
        }

        /// <summary>
        /// Print records from the current point to the end of the table.
        /// </summary>
        /// <param name="sesid">The session to use.</param>
        /// <param name="tableid">The table to dump the records from.</param>
        private void PrintRecordsToEnd(JET_SESID sesid, JET_TABLEID tableid)
        {
            do
            {
                PrintOneRow(sesid, tableid);
            }
            while (Api.TryMoveNext(sesid, tableid));
        }

        /// <summary>
        /// Print the current record and store it as a row into the datatable
        /// </summary>
        /// <param name="sesid">The session to use.</param>
        /// <param name="tableid">The table to use.</param>
        private void PrintOneRow(JET_SESID sesid, JET_TABLEID tableid)
        {
            row = dataTable.NewRow();

            for (int j = 0; j < columnNames.Length; j++)
            {
                columnid = Api.GetTableColumnid(sesid, tableid, columnNames[j]);
                Api.JetGetColumnInfo(sesid, dbid, TableName, columnNames[j], out columndef);
                VistaApi.JetGetColumnInfo(sesid, dbid, TableName, columnid, out columndefVista);

                switch (columndef.coltyp)
                {
                    
                    case JET_coltyp.Text:           row[j] = Api.RetrieveColumnAsString(sesid, tableid, columnsid[j]); break;
                    case JET_coltyp.LongText:       row[j] = Api.RetrieveColumnAsString(sesid, tableid, columnsid[j]); break;
                    case JET_coltyp.Long:           row[j] = Api.RetrieveColumnAsInt32(sesid, tableid, columnsid[j]).ToString(); break;
                    case JET_coltyp.Short:          row[j] = Api.RetrieveColumnAsInt16(sesid, tableid, columnsid[j]).ToString(); break;
                    case JET_coltyp.Binary:         row[j] = Api.RetrieveColumnAsByte(sesid, tableid, columnsid[j]).ToString(); break;//textWriter.WriteString("binary"); break; //Api.Retrei
                    case JET_coltyp.Bit:            row[j] = Api.RetrieveColumnAsByte(sesid, tableid, columnsid[j]).ToString(); break; //textWriter.WriteString("bit"); break; //Console.WriteLine((int?)Api.re(sesid,tableid,columnsid[j])); break;
                    case JET_coltyp.Currency:       row[j] = Api.RetrieveColumnAsInt16(sesid, tableid, columnsid[j]).ToString(); break;
                    case JET_coltyp.DateTime:       row[j] = Api.RetrieveColumnAsDateTime(sesid, tableid, columnsid[j]).ToString(); break;
                    case JET_coltyp.IEEEDouble:     row[j] = Api.RetrieveColumnAsDouble(sesid, tableid, columnsid[j]).ToString(); break;
                    case JET_coltyp.IEEESingle:     row[j] = Api.RetrieveColumnAsFloat(sesid, tableid, columnsid[j]).ToString(); break;
                    case JET_coltyp.LongBinary:     row[j] = Api.RetrieveColumnAsUInt32(sesid, tableid, columnsid[j]).ToString(); break;
                    case JET_coltyp.UnsignedByte:   row[j] = Api.RetrieveColumnAsByte(sesid, tableid, columnsid[j]).ToString(); break;
                    case JET_coltyp.Nil:    row[j] = Api.RetrieveColumnAsString(sesid, tableid, columnsid[j]).ToString(); break;

                    //support for missing columntypes
                    default:
                        switch (columndefVista.coltyp)
                        {
                            case VistaColtyp.LongLong: row[j] = Api.RetrieveColumnAsInt64(sesid, tableid, columnsid[j]).ToString(); break;
                            case VistaColtyp.UnsignedLong: row[j] = Api.RetrieveColumnAsUInt32(sesid, tableid, columnsid[j]).ToString(); break;
                            case VistaColtyp.UnsignedShort: row[j] = Api.RetrieveColumnAsUInt16(sesid, tableid, columnsid[j]).ToString(); break;
                            case VistaColtyp.GUID: row[j] = Api.RetrieveColumnAsGuid(sesid, tableid, columnsid[j]).ToString(); break;
                            default:
                                row[j] = "coltype:" + columndef.coltyp.ToString() + ""; break;

                        } break;
                }
               
            }
            dataTable.Rows.Add(row);
        }

        /**
         * openDatabase()
         * read all tables from the Database
         * pre: databasename, recovery (on/off) - recovery is not implement at this time
         * post: return a list with tablenames
         */
        public List<string> openDatabase(string DatabaseName, string recovery)
        {
            this.recovery = recovery;
            Console.WriteLine("Start OpenDatabase on Esent.cs");
            int dbPageSize;
            Console.WriteLine("Read DatabasePageSize");
            Api.JetGetDatabaseFileInfo(DatabaseName, out dbPageSize, JET_DbInfo.PageSize);
            Console.WriteLine("DatabasePageSize:[" + dbPageSize + "]");

            SystemParameters.DatabasePageSize = dbPageSize;
            Console.WriteLine("SystemParameter DatabasePageSize is set to " + dbPageSize + "");
            Console.WriteLine("Opening a new database");

            //clear List with any previous TableNames before we add new TableNames to the list.
            tables.Clear();

            //Start instance
            using (var instance = new Instance("ESE_Examination"))
            {
                Console.WriteLine("Created a new instance for " + DatabaseName);
                // Creating an Instance object doesn't call JetInit. 
                // This is done to allow some parameters to be set
                // before the instance is initialized.
                // Api.JetSetSystemParameter(instance, JET_SESID.Nil, JET_param.MaxTemporaryTables, 0, null);


                Console.WriteLine("Recovery of DB is set to " + this.recovery);
                Api.JetSetSystemParameter(instance, JET_SESID.Nil, JET_param.Recovery, 0, "off");

                instance.Parameters.CircularLog = true;

                // Initialize the instance. This creates the logs and temporary database.
                // If logs are present in the log directory then recovery will run
                // automatically.
                ///instance.Parameters.PageTempDBMin = 8192;
                Console.WriteLine("Initializing instance...");
                instance.Init();
                Console.WriteLine("Instance initialized!");

                // Create a disposable wrapper around the JET_SESID.
                using (var session = new Session(instance))
                {
                    Console.WriteLine("Created a new session");
                    JET_DBID dbid;
                    Boolean exception = false;
                    //Error PageSizeMismatch (JET_errPageSizeMismatch, The database page size does not match the engine)

                    // The database only has to be attached once per instance, but each
                    // session has to open the database. Redundant JetAttachDatabase calls
                    // are safe to make though.
                    // Here we use the fact that Instance, Session and Table object all have
                    // implicit conversions to the underlying JET_* types. This allows the
                    // disposable wrappers to be used with any APIs that expect the JET_*
                    // structures.

                    Console.WriteLine("Trying to attach the database to session");
                    try
                    {
                        //AttachDatabase is set to READ ONLY, we don't want to manipulate any files
                        Api.JetAttachDatabase(session, DatabaseName, AttachDatabaseGrbit.ReadOnly);
                        Console.WriteLine("Database attached to session");
                    }
                    catch (EsentDirtyShutdownException edse)
                    {
                        Console.WriteLine("Database DirtyShutdown Exception. Try to repair database first" + edse.Message);
                        exception = true;

                    }
                    catch (EsentDatabaseDirtyShutdownException eddse)
                    {
                        Console.WriteLine("Database DirtyShutdown Exception. Try to repair database first" + eddse.Message);
                        exception = true;
                    }

                    if (!exception)
                    {
                        Console.WriteLine("Trying to open the database");
                        //OpenDatabase is set to READ ONLY, we don't want to manipulate any files
                        Api.JetOpenDatabase(session, DatabaseName, null, out dbid, OpenDatabaseGrbit.ReadOnly);
                        Console.WriteLine("Database succesfully opened!");

                        Console.WriteLine("Get all tablenames from database");
                        foreach (string tablename in Api.GetTableNames(session, dbid))
                        {
                            tables.Add(tablename);
                        }
                    }
                }
                Console.WriteLine("Closing session");
            }
            Console.WriteLine("Closing instance");

           return tables;
        }
    }
}

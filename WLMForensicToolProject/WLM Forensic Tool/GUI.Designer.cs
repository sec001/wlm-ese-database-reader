﻿/*
  GUI.Designer.cs
	
  Graphical User Interface Designer
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: GUI.Designer.cs, Joeri D. Blokhuis Exp $
 */
namespace WindowsFormsApplication1
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GUI));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gdfgToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToCSVCurrentTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportContactTreeToCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToXMLAllTablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToXMLCurrentTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportContactTreeToXMLToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.highlight_lastsearch = new System.Windows.Forms.CheckBox();
            this.checkBox1_SearchTables = new System.Windows.Forms.CheckBox();
            this.checkBox1_Match = new System.Windows.Forms.CheckBox();
            this.searchResultsLabel = new System.Windows.Forms.Label();
            this.searchbutton = new System.Windows.Forms.Button();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.exportXMLbutton = new System.Windows.Forms.Button();
            this.openFileButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridCompare1 = new System.Windows.Forms.DataGridView();
            this.dataGridCompare2 = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.openFileCompare1 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.openFileCompare2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ESETables = new System.Windows.Forms.TabPage();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.ese_tablename_view = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.licensToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCompare1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCompare2)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.ESETables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ese_tablename_view)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 468);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(119, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gdfgToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gdfgToolStripMenuItem
            // 
            this.gdfgToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.gdfgToolStripMenuItem.Name = "gdfgToolStripMenuItem";
            this.gdfgToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.gdfgToolStripMenuItem.Text = "File";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openFileToolStripMenuItem.Image")));
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openFileToolStripMenuItem.Text = "Open";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuriToolStripMenuItem,
            this.exportToCSVCurrentTableToolStripMenuItem,
            this.exportContactTreeToCSVToolStripMenuItem,
            this.exportToXMLAllTablesToolStripMenuItem,
            this.exportToXMLCurrentTableToolStripMenuItem,
            this.exportContactTreeToXMLToolStripMenuItem1});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.settingsToolStripMenuItem.Text = "Export";
            // 
            // configuriToolStripMenuItem
            // 
            this.configuriToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("configuriToolStripMenuItem.Image")));
            this.configuriToolStripMenuItem.Name = "configuriToolStripMenuItem";
            this.configuriToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.configuriToolStripMenuItem.Text = "Export to CSV (All Tables)";
            this.configuriToolStripMenuItem.Click += new System.EventHandler(this.configuriToolStripMenuItem_Click);
            // 
            // exportToCSVCurrentTableToolStripMenuItem
            // 
            this.exportToCSVCurrentTableToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportToCSVCurrentTableToolStripMenuItem.Image")));
            this.exportToCSVCurrentTableToolStripMenuItem.Name = "exportToCSVCurrentTableToolStripMenuItem";
            this.exportToCSVCurrentTableToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.exportToCSVCurrentTableToolStripMenuItem.Text = "Export to CSV (Current Table)";
            this.exportToCSVCurrentTableToolStripMenuItem.Click += new System.EventHandler(this.exportToCSVCurrentTableToolStripMenuItem_Click);
            // 
            // exportContactTreeToCSVToolStripMenuItem
            // 
            this.exportContactTreeToCSVToolStripMenuItem.Image = global::EsentReader.Properties.Resources.csv_icon;
            this.exportContactTreeToCSVToolStripMenuItem.Name = "exportContactTreeToCSVToolStripMenuItem";
            this.exportContactTreeToCSVToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.exportContactTreeToCSVToolStripMenuItem.Text = "Export ContactTree to CSV";
            this.exportContactTreeToCSVToolStripMenuItem.Click += new System.EventHandler(this.exportContactTreeToCSVToolStripMenuItem_Click);
            // 
            // exportToXMLAllTablesToolStripMenuItem
            // 
            this.exportToXMLAllTablesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportToXMLAllTablesToolStripMenuItem.Image")));
            this.exportToXMLAllTablesToolStripMenuItem.Name = "exportToXMLAllTablesToolStripMenuItem";
            this.exportToXMLAllTablesToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.exportToXMLAllTablesToolStripMenuItem.Text = "Export to XML(All Tables)";
            this.exportToXMLAllTablesToolStripMenuItem.Click += new System.EventHandler(this.exportToXMLAllTablesToolStripMenuItem_Click);
            // 
            // exportToXMLCurrentTableToolStripMenuItem
            // 
            this.exportToXMLCurrentTableToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportToXMLCurrentTableToolStripMenuItem.Image")));
            this.exportToXMLCurrentTableToolStripMenuItem.Name = "exportToXMLCurrentTableToolStripMenuItem";
            this.exportToXMLCurrentTableToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.exportToXMLCurrentTableToolStripMenuItem.Text = "Export to XML (Current Table)";
            // 
            // exportContactTreeToXMLToolStripMenuItem1
            // 
            this.exportContactTreeToXMLToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("exportContactTreeToXMLToolStripMenuItem1.Image")));
            this.exportContactTreeToXMLToolStripMenuItem1.Name = "exportContactTreeToXMLToolStripMenuItem1";
            this.exportContactTreeToXMLToolStripMenuItem1.Size = new System.Drawing.Size(231, 22);
            this.exportContactTreeToXMLToolStripMenuItem1.Text = "Export ContactTree to XML";
            this.exportContactTreeToXMLToolStripMenuItem1.Click += new System.EventHandler(this.exportContactTreeToXMLToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.licensToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(0, 76);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1008, 88);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "File properties";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(294, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Amount of tables: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "File size: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "File name: ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.highlight_lastsearch);
            this.panel2.Controls.Add(this.checkBox1_SearchTables);
            this.panel2.Controls.Add(this.checkBox1_Match);
            this.panel2.Controls.Add(this.searchResultsLabel);
            this.panel2.Controls.Add(this.searchbutton);
            this.panel2.Controls.Add(this.searchBox);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.exportXMLbutton);
            this.panel2.Controls.Add(this.openFileButton);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.resetButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 566);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1008, 164);
            this.panel2.TabIndex = 11;
            // 
            // highlight_lastsearch
            // 
            this.highlight_lastsearch.AutoSize = true;
            this.highlight_lastsearch.Location = new System.Drawing.Point(834, 6);
            this.highlight_lastsearch.Name = "highlight_lastsearch";
            this.highlight_lastsearch.Size = new System.Drawing.Size(121, 17);
            this.highlight_lastsearch.TabIndex = 16;
            this.highlight_lastsearch.Text = "Highlight last search";
            this.highlight_lastsearch.UseVisualStyleBackColor = true;
            // 
            // checkBox1_SearchTables
            // 
            this.checkBox1_SearchTables.AutoSize = true;
            this.checkBox1_SearchTables.Location = new System.Drawing.Point(733, 6);
            this.checkBox1_SearchTables.Name = "checkBox1_SearchTables";
            this.checkBox1_SearchTables.Size = new System.Drawing.Size(95, 17);
            this.checkBox1_SearchTables.TabIndex = 15;
            this.checkBox1_SearchTables.Text = "All open tables";
            this.checkBox1_SearchTables.UseVisualStyleBackColor = true;
            // 
            // checkBox1_Match
            // 
            this.checkBox1_Match.AutoSize = true;
            this.checkBox1_Match.Location = new System.Drawing.Point(671, 6);
            this.checkBox1_Match.Name = "checkBox1_Match";
            this.checkBox1_Match.Size = new System.Drawing.Size(56, 17);
            this.checkBox1_Match.TabIndex = 14;
            this.checkBox1_Match.Text = "Match";
            this.checkBox1_Match.UseVisualStyleBackColor = true;
            // 
            // searchResultsLabel
            // 
            this.searchResultsLabel.AutoSize = true;
            this.searchResultsLabel.Location = new System.Drawing.Point(668, 57);
            this.searchResultsLabel.Name = "searchResultsLabel";
            this.searchResultsLabel.Size = new System.Drawing.Size(0, 13);
            this.searchResultsLabel.TabIndex = 13;
            // 
            // searchbutton
            // 
            this.searchbutton.Location = new System.Drawing.Point(880, 24);
            this.searchbutton.Name = "searchbutton";
            this.searchbutton.Size = new System.Drawing.Size(75, 23);
            this.searchbutton.TabIndex = 12;
            this.searchbutton.Text = "search";
            this.searchbutton.UseVisualStyleBackColor = true;
            this.searchbutton.Click += new System.EventHandler(this.searchbutton_Click);
            // 
            // searchBox
            // 
            this.searchBox.Location = new System.Drawing.Point(671, 26);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(193, 20);
            this.searchBox.TabIndex = 11;
            this.searchBox.Enter += new System.EventHandler(this.searchBox_Enter);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.Location = new System.Drawing.Point(592, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 68);
            this.button2.TabIndex = 9;
            this.button2.Text = "Export";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // exportXMLbutton
            // 
            this.exportXMLbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("exportXMLbutton.BackgroundImage")));
            this.exportXMLbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.exportXMLbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportXMLbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.exportXMLbutton.Location = new System.Drawing.Point(522, 2);
            this.exportXMLbutton.Name = "exportXMLbutton";
            this.exportXMLbutton.Size = new System.Drawing.Size(64, 68);
            this.exportXMLbutton.TabIndex = 5;
            this.exportXMLbutton.Tag = "Export ESE to XML";
            this.exportXMLbutton.Text = "export";
            this.exportXMLbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.exportXMLbutton.UseVisualStyleBackColor = true;
            this.exportXMLbutton.Click += new System.EventHandler(this.button2_Click);
            // 
            // openFileButton
            // 
            this.openFileButton.BackgroundImage = global::EsentReader.Properties.Resources._48px_Document_open_svg;
            this.openFileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.openFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openFileButton.Location = new System.Drawing.Point(452, 2);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(64, 68);
            this.openFileButton.TabIndex = 0;
            this.openFileButton.Text = "open";
            this.openFileButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // resetButton
            // 
            this.resetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetButton.Image = global::EsentReader.Properties.Resources.undo;
            this.resetButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.resetButton.Location = new System.Drawing.Point(382, 2);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(64, 68);
            this.resetButton.TabIndex = 7;
            this.resetButton.Tag = "clear tabs";
            this.resetButton.Text = "reset";
            this.resetButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1000, 516);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "WLM Contact Categories";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1000, 516);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Compare";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 103);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridCompare1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridCompare2);
            this.splitContainer2.Size = new System.Drawing.Size(994, 410);
            this.splitContainer2.SplitterDistance = 493;
            this.splitContainer2.TabIndex = 1;
            // 
            // dataGridCompare1
            // 
            this.dataGridCompare1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCompare1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridCompare1.Location = new System.Drawing.Point(0, 0);
            this.dataGridCompare1.Name = "dataGridCompare1";
            this.dataGridCompare1.Size = new System.Drawing.Size(493, 410);
            this.dataGridCompare1.TabIndex = 0;
            // 
            // dataGridCompare2
            // 
            this.dataGridCompare2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCompare2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridCompare2.Location = new System.Drawing.Point(0, 0);
            this.dataGridCompare2.Name = "dataGridCompare2";
            this.dataGridCompare2.Size = new System.Drawing.Size(497, 410);
            this.dataGridCompare2.TabIndex = 0;
            this.dataGridCompare2.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Grid_Scrolled);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.panel3);
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(994, 100);
            this.panel5.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.openFileCompare1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(143, 100);
            this.panel4.TabIndex = 6;
            // 
            // openFileCompare1
            // 
            this.openFileCompare1.Location = new System.Drawing.Point(40, 30);
            this.openFileCompare1.Name = "openFileCompare1";
            this.openFileCompare1.Size = new System.Drawing.Size(72, 23);
            this.openFileCompare1.TabIndex = 0;
            this.openFileCompare1.Text = "Open Left";
            this.openFileCompare1.UseVisualStyleBackColor = true;
            this.openFileCompare1.Click += new System.EventHandler(this.openFileCompare1_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.openFileCompare2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(838, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(156, 100);
            this.panel3.TabIndex = 5;
            // 
            // openFileCompare2
            // 
            this.openFileCompare2.Location = new System.Drawing.Point(34, 30);
            this.openFileCompare2.Name = "openFileCompare2";
            this.openFileCompare2.Size = new System.Drawing.Size(75, 23);
            this.openFileCompare2.TabIndex = 1;
            this.openFileCompare2.Text = "Open Right";
            this.openFileCompare2.UseVisualStyleBackColor = true;
            this.openFileCompare2.Click += new System.EventHandler(this.openFileCompare2_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.listBox1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(384, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(350, 100);
            this.panel1.TabIndex = 4;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.richTextBox1.Location = new System.Drawing.Point(196, 13);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(154, 87);
            this.richTextBox1.TabIndex = 7;
            this.richTextBox1.Text = "Move the scrollbars of the right table to have a synced view";
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(0, 13);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(190, 82);
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Select a tablename to compare";
            // 
            // ESETables
            // 
            this.ESETables.Controls.Add(this.splitter1);
            this.ESETables.Controls.Add(this.tabControl2);
            this.ESETables.Controls.Add(this.ese_tablename_view);
            this.ESETables.Location = new System.Drawing.Point(4, 22);
            this.ESETables.Name = "ESETables";
            this.ESETables.Padding = new System.Windows.Forms.Padding(3);
            this.ESETables.Size = new System.Drawing.Size(1000, 516);
            this.ESETables.TabIndex = 0;
            this.ESETables.Text = "Database";
            this.ESETables.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(349, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 510);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // tabControl2
            // 
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(349, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(648, 510);
            this.tabControl2.TabIndex = 1;
            // 
            // ese_tablename_view
            // 
            this.ese_tablename_view.AllowUserToAddRows = false;
            this.ese_tablename_view.AllowUserToDeleteRows = false;
            this.ese_tablename_view.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.ese_tablename_view.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.ese_tablename_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ese_tablename_view.Dock = System.Windows.Forms.DockStyle.Left;
            this.ese_tablename_view.Location = new System.Drawing.Point(3, 3);
            this.ese_tablename_view.Name = "ese_tablename_view";
            this.ese_tablename_view.ReadOnly = true;
            this.ese_tablename_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.ese_tablename_view.Size = new System.Drawing.Size(346, 510);
            this.ese_tablename_view.TabIndex = 0;
            this.ese_tablename_view.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleName = "";
            this.tabControl1.AllowDrop = true;
            this.tabControl1.Controls.Add(this.ESETables);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1008, 542);
            this.tabControl1.TabIndex = 9;
            // 
            // licensToolStripMenuItem
            // 
            this.licensToolStripMenuItem.Name = "licensToolStripMenuItem";
            this.licensToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.licensToolStripMenuItem.Text = "License";
            this.licensToolStripMenuItem.Click += new System.EventHandler(this.licensToolStripMenuItem_Click);
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WLM ESE Examiner";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCompare1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCompare2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ESETables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ese_tablename_view)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gdfgToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button exportXMLbutton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuriToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dataGridCompare1;
        private System.Windows.Forms.DataGridView dataGridCompare2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button openFileCompare2;
        private System.Windows.Forms.Button openFileCompare1;
        private System.Windows.Forms.TabPage ESETables;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.DataGridView ese_tablename_view;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem exportToCSVCurrentTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToXMLAllTablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToXMLCurrentTableToolStripMenuItem;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem exportContactTreeToXMLToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportContactTreeToCSVToolStripMenuItem;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.Button searchbutton;
        private System.Windows.Forms.Label searchResultsLabel;
        private System.Windows.Forms.CheckBox checkBox1_Match;
        private System.Windows.Forms.CheckBox checkBox1_SearchTables;
        private System.Windows.Forms.CheckBox highlight_lastsearch;
        private System.Windows.Forms.ToolStripMenuItem licensToolStripMenuItem;
    }
}


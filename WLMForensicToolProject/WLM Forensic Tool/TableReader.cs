﻿/*
  TableReader.cs
	
  Returns all tables in a database in a datatable
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: TableReader.cs, Joeri D. Blokhuis Exp $
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace WindowsFormsApplication1
{
    class TableReader
    {
        private DataRow myNewRow;
        private DataColumn column;
        private DataTable datatable;

        /**
         *  Create table for columns in ESE database
         *  Two columns are created: 1) open/close tables
         *  2) Tablename
         *  
         * @param names - names of each table in an ESE db
         * @param columnName - name of the second column. Filename can be used.
         * 
         * return datatable
         */

        public DataTable GetResultsTable(List<string> names, String columnName)
        {
            // Create the output table.
            datatable = new DataTable();

            // Create first column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Boolean");
            column.ColumnName = "Open/Close";
            column.AutoIncrement = false;
            column.Caption = "test";
            column.ReadOnly = false;
            column.Unique = false;
            
            // Add the column to the table.
            datatable.Columns.Add(column);

            // Create second column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = columnName;
            column.AutoIncrement = false;
            column.Caption = columnName;
            column.ReadOnly = true;
            column.Unique = false;
           
            // Add the column to the table.
            datatable.Columns.Add(column);

            // Populate rows with its values.
            foreach(String item in names){
                myNewRow = datatable.NewRow();   
                myNewRow[0] = false;
                myNewRow[1] = item;
                datatable.Rows.Add(myNewRow);
            }

            return datatable;
        }

    }
}

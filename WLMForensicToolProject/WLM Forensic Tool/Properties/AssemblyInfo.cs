﻿/*
  AssemblyInfo.cs
	
  Assembly Information of WLM ESE Examiner
	
  Copyright (c) 2011 by Wouter S. van Dongen and Joeri D. Blokhuis
  wouter@dongit.nl
  jblokhuis@os3.nl
  http://sourceforge.net/projects/wlmeseexaminer/
	
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3 as published by
  the Free Software Foundation;
	
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
	
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  (See COPYING.txt)

  $Id: AssemblyInfo.cs, Joeri D. Blokhuis Exp $
 */
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WLM ESE Examiner")]
[assembly: AssemblyDescription("Reads any Extensible Storage Engine(ESE) database files(.edb). Uses of ESE databases are found in Windows Live Mail/Messenger/Desktop Search/Calendar/etc. WLM ESE Examiner detects Windows Live Messenger database files and shows all user contacts in the corresponding category. Further it is possible to compare database files, search on any field and export to CSV and XML file formats. This program is ready to use and continued in its developments. Any suggestions/comments are more than welcome.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("W.S. van Dongen (wouter@dongit.nl) and J.D. Blokhuis (jblokhuis@os3.nl)")]
[assembly: AssemblyProduct("-")]
[assembly: AssemblyCopyright("Copyright ©  2011")]
[assembly: AssemblyTrademark("-")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("04005a37-53c0-4fa9-b8f1-9ffc1db4963c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
